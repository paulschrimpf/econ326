\input{../slideHeader}

\title{Introduction}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 326}
\date{\today}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

\begin{frame}
  \tableofcontents  
\end{frame}

\section{What is Econometrics?}

\begin{frame}\frametitle{What is Econometrics?}
  Econometrics is concerned with the development of statistical
  methods for:
  \begin{itemize}
  \item Estimation of economic relationships
  \item Testing of economic theories
  \item Forecasting of important economic variables
  \item Evaluation of government and business policy
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Why statistics?}
  \begin{itemize}
  \item Economic theory is used to construct models characterizing
    relationships between variables of interest
  \item Economic models are only approximations 
  \item A model can take into account a number of important factors
    but there will be many factors left out that also affect
    outcomes
  \item We therefore replace the exact (deterministic) model with a
    probabilistic model
  \end{itemize}
\end{frame}

\subsection{Examples}

\begin{frame}[shrink]
  \frametitle{Example: Mincer's (1974) wage regression}
  \begin{itemize}
  \item Economic Model: wage depends on human capital
    \[
    \mathrm{wage} = f \left(\mathrm{educ}, \mathrm{exper},\mathrm{training}
    \right) \]
  \item Econometric Model: 
    {\small{
    \[
    \log(\mathrm{wage}) = \beta_0 + \beta_1 \mathrm{educ} + \beta_2 \mathrm{exper}
    + \beta_3 (\mathrm{exper})^2 + \beta_4 \mathrm{training} +u 
    \]
  }}
The term u captures unobserved factors:
\begin{itemize}
\item innate ability, 
\item family background, 
\item  quality of education
\end{itemize}
\item Hypothesis Testing: whether training affects wage $H_0 : \beta_4 = 0$
\end{itemize}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Example: Becker's (1968) economic model of crime}
  \begin{itemize}
  \item Economic Model: the reward from property crime vs. the cost
    \[ y = f(x_1, x_2, x_3, x_4, ...) \]
    \begin{itemize}
    \item $y = $hours spent in criminal activities (crime)
    \item $x_1 =$ wage for an hour spent in criminal activities
    \item $x_2 =$ wage in legal employment (wagem)
    \item $x_3 =$ income other than from crime or employment (othinc)
    \item $x_4 =$ probability of getting caught (freqarr)
    \end{itemize}
  \item Econometric Model:
    \[ \mathrm{crime} = \beta_0 + \beta_1 \mathrm{wagem} + \beta_2
    \mathrm{othinc} + \beta_3 \mathrm{freqarr} + u \]
    The term $u$ captures unobserved factors such as:
    \begin{itemize}
    \item the reward for criminal activity, 
    \item  family background, 
    \item measurement error
    \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Causality}

\begin{frame}
  \frametitle{Causality}
  \begin{itemize}
  \item We are interested in causal relations, but statistics only allows us
    to establish correlations 
  \item To say that one variable has a causal effect on
    another, other factors affecting the outcome must be held
    fixed 
  \item Sometimes can use controlled experiments
  \item Experiment are often impossible in economics (too costly
    and/or for ethical reasons)
  \item Must rely on observational data
  \end{itemize}
\end{frame}

\subsection{Potential Outcomes}

\begin{frame}
  \begin{itemize}
  \item Example: effect of health insurance coverage on health
  \item Question: what is the effect of health insurance on health?
  \item Ideal experiment: randomly give people health insurance or
    not, compare their health afterward
  \item Observed survey data: \\
    \begin{tabular}{lccc} 
      \hline
      Group & Sample Size & Mean Health & Std.Dev. \\
      Some insurance & 8114 & 4.01 & 0.93 \\
      No insurance & 1281 & 3.70 & 1.01 \\ 
      \hline
    \end{tabular} \\
    {\footnotesize{Source: 2009 NHIS data reported by \cite{ap2014}}}
  \item Is 4.01 - 3.70 = 0.31 the causal effect of health insurance
    on health?
  \item Not likely, people with and without insurance differ in other
    ways that might affect health
    \begin{tabular}{lccc} 
      \hline
      Group &  Mean Education & Mean income \\
      Some insurance & 14.31 & 106,467  \\
      No insurance & 11.56 &  45,656 \\ 
      \hline
    \end{tabular} \\
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Potential Outcomes}
  \begin{itemize}
  \item Potential outcomes (aka the Rubin causal model) are one
    powerful way of thinking about causality
  \item Let $D_i = 1$ if person $i$ has health insurance, $0$ otherwise
  \item Let $Y_{i0}$ and $Y_{i1}$ be the \textbf{potential outcomes}
    \begin{itemize}
    \item $Y_{i0} =$ health if person $i$ does not have health insurance
    \item $Y_{i1} =$ health if person $i$ has health insurance
    \end{itemize}
  \item We observe 
    \begin{align*}
      Y_i = & \begin{cases} Y_{i0} & \text{ if } D_i = 0 \\
        Y_{i1} & \text{ if } D_i = 1 \end{cases} \\
      & = (1 - D_i) Y_{i0} +
      D_i Y_{i1} \\
      = & Y_{i0} + D_i \times \underbrace{(Y_{i1} - Y_{i0})}_{\text{causal effect
          of health insurance for person }i}
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Potential Outcomes (continued)}
  \begin{itemize}
  \item Observed average difference in health 
    \begin{align*}
      \Er[Y_{i1} | D_i=1] - & \Er[Y_{i0} | D_i = 0] = \\ = & 
      \underbrace{\Er[Y_{i1} - Y_{i0} | D_i = 1]}_{\text{Average treatment
        effect on the treated}} + \\ & +
      \underbrace{\Er[Y_{i0}|D_i=1] - \Er[Y_{i0} | D_i =
        0]}_{\text{Selection bias}} 
    \end{align*}
  \item Insurance may help you or hurt you (on average), but wealthier
    people are more likely to afford insurance and might be healthier
    for other reasons; conversely people who are sick might be more
    willing to pay for insurance
  \item Random assignment of $D_i$ removes selection bias because
    $D_i$ and $Y_{i0}$ are then independent
  \item Often cannot randomize, but can 
    \begin{itemize}
    \item Use available data to approximate desired experiment
    \item Use economic theory to impose restrictions
    \end{itemize}
    \note{Cannot randomize everything, perhaps not hospitalization for
      routine medical complaints, but you can try to use the data you
      have (or collect more) in an effort to come close to the desired
      experiment. The details of how this is done is what most of
      econometrics is about. 
          
      Economic theory does not say a whole lot about hospitalization,
      but it can tell us something. Going to the hospital costs time
      and money. People only go to the hospital if the expected
      benefit from the change in health is greater than the cost. If
      the benefit decreases as $Y_{i0}$ increases, then we should
      expect $\Er[Y_{i0}|D_i=0] \geq \Er[Y_{i0} | D_i = 1]$. In that
      case, the observed difference in health is a lower bound for the
      ATT. 
    }
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Examples}

\begin{frame}
  \frametitle{Examples}
  \begin{itemize}
  \item \textbf{Education} 
    \[
    \log (\text{Wage}) = \alpha + \beta\times \text{Years of
      Schooling} + U,
    \]
    U = other factors, for example, ability. Since it is very hard to
    control for ability, one can overestimate the return to
    education by relying on usual correlations.
  \item \textbf{Minimum wage and employment} (Card and Krueger
    {\slshape AER} 1994)
    \[ \text{Unemployment} = \beta_0 + \beta_1 \times \text{Minium
      Wage} + U \] 
    Reverse causality: High employment may lead to political
    pressure for higher minimum wage.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Examples (continued)}
  \begin{itemize}
  \item \textbf{Size of the police force and crime} 
    \[ \text{Number of Crimes} = \alpha + \beta \times \text{Size of
      the Police Force} + U
    \]
    Usually, cities with a lot of criminal activity have a bigger
    police force. Simple correlations can spuriously indicate that
    the size of the police force has a positive effect on the crime
    rates.
  %\item \textbf{Sex on TV and teen pregnancy} (Chandra et al,
  %  {\slshape Pediatrics}, 2008)
  %  \begin{align*}
  %    \text{Prob } & \text{of teen pregnancy} = \\
  %    = & F (\beta_0 + \beta_1 \times \text{Exposure to Sex on TV} +
  %    \beta_2 \times \text{Total TV}) 
  %  \end{align*}
  %  Self-selection : it is hard to control for interest in sex, no
  %  causal relationship.
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \bibliographystyle{jpe}
  \bibliography{../326}
\end{frame}


\end{document}

