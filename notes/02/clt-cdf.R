rm(list=ls()) ## clears R's workspace
graphics.off() ## closes any open graphs

## load.fun(x) will load library 'x' if it is installed. If 'x' has
## not been installed, it will install it. Understanding this code is
## not necessary
## source: http://r.789695.n4.nabble.com/Install-package-automatically-if-not-there-tp2267532p2267659.html
load.fun <- function(x) {
  x <- as.character(substitute(x))
  if(isTRUE(x %in% .packages(all.available=TRUE))) {
    eval(parse(text=paste("require(", x, ")", sep="")))
  } else {
    #update.packages()  ## good idea, but may take some time. can
                        ## usually be safely skipped
    eval(parse(text=paste("install.packages('", x, "')", sep="")))
    eval(parse(text=paste("require(", x, ")", sep="")))
  }
}

## load needed packages
load.fun(ggplot2)
load.fun(reshape)

## settings
N <- c(2,5,10,15,20)
simulations <- 50000
rdist <- function(n) {
  ##rbeta(n,shape1=0.5, shape2=0.5)
  runif(n)
}
mu <- 0.5 ## mean of rdist

x <- seq(-3,3,length.out=300)
cdf <- matrix(0,nrow=length(x),ncol=length(N))
cdfp <- matrix(0,nrow=length(x),ncol=length(N))
for(i in 1:length(N)) { ## loop over sample sizes
  n <- N[i]
  dat <- matrix(rdist(n*simulations),
                nrow=simulations, ncol=n)
  zn <- sqrt(n)*(apply(dat, 1, mean) - mu)/(apply(dat, 1, sd))
  cdf[,i] <- sapply(x, function (y) { mean(zn<=y) } )

  ## or in slower loop form
  ##for(j in 1:length(x)) {
  ##   cdf[j,i] <- mean(zn<=x[j])
  ##}
  cdfp[,i] <- sapply(x, function (y) { mean(pnorm(zn)<=pnorm(y)) } )
}

## Graphing cdf
df <- data.frame(cdf)
df$phix <- pnorm(x)
df$z <- x
df <- melt(df,id.vars=c("z"))

cltPlot <- ggplot(data=df, aes(x=z, y=value, group=variable, color=variable)) +
  geom_line() +
  scale_x_continuous(name=expression(z)) + ## label x-axis
  scale_y_continuous(name="CDF") +
  scale_colour_brewer(type="div",palette="RdYlGn",
                      name="N",label=c(N,expression(infty))) + ## controls colors of histograms
  theme_minimal()
cltPlot ## display the plot
cdf.cltPlot <- cltPlot
## saves the plot as pdf
pdf("clt-cdf.pdf")
cltPlot
dev.off()

## Graphing p-values
df <- data.frame(cdfp)
df$phix <- pnorm(x)
df$z <- pnorm(x)
df <- melt(df,id.vars=c("z"))
cltPlot <- ggplot(data=df, aes(x=z, y=value, group=variable, color=variable)) +
  geom_line() +
  scale_x_continuous(name="nominal size") + ## label x-axis
  scale_y_continuous(name="actual size") +
  scale_colour_brewer(type="div",palette="RdYlGn",
                      name="N",label=c(N,expression(infty)))  + ## controls colors of histograms
  theme_minimal()
cltPlot ## display the plot

## saves the plot as pdf
pdf("clt-p.pdf")
cltPlot
dev.off()
