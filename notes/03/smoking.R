rm(list=ls())
graphics.off()
fdims <-1.5*c(5.03937, 3.77953) ## figure dimensions, =128mm x 96mm = dimensions of beamer slide
## For labeling, see http://stackoverflow.com/a/13451587
lm_eqn = function(m) {
  l <- list(a = format(coef(m)[1], digits = 2),
            b = format(abs(coef(m)[2]), digits = 2))
  if (coef(m)[2] >= 0)  {
    eq <- substitute(italic(y) == a + b %.% italic(x),l)
  } else {
    eq <- substitute(italic(y) == a - b %.% italic(x),l)
  }

  as.character(as.expression(eq));
}


if (!file.exists("cigcancerdat.html")) {
  download.file(url="http://lib.stat.cmu.edu/DASL/Datafiles/cigcancerdat.html",
                destfile="cigcancerdat.html")
}
## note: you must manually delete the last line of cigcancerdat.html
## or this won't work
smoke <- read.table("cigcancerdat.html", header=FALSE,
                    sep="\t",skip=37)
names(smoke) <- c("State","cig","bladder","lung","kidney","leukemia")

reg <- lm(bladder ~ cig, data=smoke)
require(ggplot2)
p <- ggplot(data = smoke, aes(x = cig, y = bladder)) +
  geom_smooth(method = "lm", se=FALSE, color="red", formula = y ~ x) +
    geom_text(aes(label=State),size=4) +
  geom_text(data=data.frame(),aes(x=quantile(smoke$cig,0.1),
                y=quantile(smoke$bladder,0.9)),
            label=lm_eqn(reg), parse=TRUE)
pdf("cigBladder.pdf",width=fdims[1],height=fdims[2])
p
dev.off()
models <- list()
models[[1]] <- reg

reg <- lm(lung ~ cig, data=smoke)
p <- ggplot(data = smoke, aes(x = cig, y = lung)) +
  geom_smooth(method = "lm", se=FALSE, color="red", formula = y ~ x) +
    geom_text(aes(label=State),size=4) +
  geom_text(data=data.frame(),aes(x=quantile(smoke$cig,0.1),
                y=quantile(smoke$lung,0.9)),
            label=lm_eqn(reg), parse=TRUE)
pdf("cigLung.pdf",width=fdims[1],height=fdims[2])
p
dev.off()
models[[2]] <- reg

reg <- lm(kidney ~ cig, data=smoke)
p <- ggplot(data = smoke, aes(x = cig, y = kidney)) +
  geom_smooth(method = "lm", se=FALSE, color="red", formula = y ~ x) +
    geom_text(aes(label=State),size=4) +
  geom_text(data=data.frame(),aes(x=quantile(smoke$cig,0.1),
                y=quantile(smoke$kidney,0.9)),
            label=lm_eqn(reg), parse=TRUE)
pdf("cigKidney.pdf",width=fdims[1],height=fdims[2])
p
dev.off()
models[[3]] <- reg

reg <- lm(leukemia ~ cig, data=smoke)
p <- ggplot(data = smoke, aes(x = cig, y = leukemia)) +
  geom_smooth(method = "lm", se=FALSE, color="red", formula = y ~ x) +
    geom_text(aes(label=State),size=4) +
  geom_text(data=data.frame(),aes(x=quantile(smoke$cig,0.1),
                y=quantile(smoke$leukemia,0.9)),
            label=lm_eqn(reg), parse=TRUE)
pdf("cigLeukemia.pdf",width=fdims[1],height=fdims[2])
p
dev.off()
models[[4]] <- reg

## create nice LaTeX table of regression results
require(texreg)
texTable <-  texreg(models,
                    model.names=c("Bladder", "Lung", "Kidney",
                      "Leukemia"), use.packages=FALSE,
                    caption="Smoking and cancer", label="tab:smoking",return.string=TRUE)
cat(texTable,file="smoking.tex")

