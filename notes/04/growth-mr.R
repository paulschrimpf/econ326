rm(list=ls())
graphics.off()
require(ggplot2)
require(foreign)
## For labeling, see http://stackoverflow.com/a/13451587
lm_eqn = function(m) {
  l <- list(a = format(coef(m)[1], digits = 2),
            b = format(abs(coef(m)[2]), digits = 2))
  if (coef(m)[2] >= 0)  {
    eq <- substitute(italic(y) == a + b %.% italic(x),l)
  } else {
    eq <- substitute(italic(y) == a - b %.% italic(x),l)
  }

  as.character(as.expression(eq));
}


if (!file.exists("Growth.dta")) {
  download.file(url="http://wps.aw.com/wps/media/objects/3254/3332253/datasets2e/datasets/Growth.dta",
                destfile="Growth.dta", mode="wb")
}
gdf <- read.dta("Growth.dta")
gdf$rgdp60 <- gdf$rgdp60/1000

greg <- lm(growth ~ rgdp60 + yearsschool, data=gdf)

## plot bivariate regressions
reg.gdp <- lm(growth ~ rgdp60, data=gdf)
p <- ggplot(data = gdf, aes(x = rgdp60, y = growth)) +
  geom_smooth(method = "lm", color="red", formula = y ~ x) +
  geom_text(aes(label=country_name),size=4,hjust=0.5,vjust=1,angle=45) +
  geom_text(data=data.frame(),aes(x=quantile(gdf$rgdp60,0.8),
                y=quantile(gdf$growth,0.99)),
            label=lm_eqn(reg.gdp), parse=TRUE) +
  geom_point(aes(),colour="red")
dev.new("growth-GDP")
p

reg.sch <- lm(growth ~ yearsschool, data=gdf)
p <- ggplot(data = gdf, aes(x = yearsschool, y = growth)) +
  geom_smooth(method = "lm", color="red", formula = y ~ x) +
  geom_text(aes(label=country_name),size=4,hjust=0.5,vjust=1,angle=45) +
  geom_text(data=data.frame(),aes(x=7.5,
                y=quantile(gdf$growth,0.99)),
            label=lm_eqn(reg.sch), parse=TRUE) +
  geom_point(aes(),colour="red")
dev.new("growth-school")
p

## plot multiple regression
require(rgl)
## create plot using Rgl
rgl.clear("all")
open3d()
rgl.spheres(gdf$rgdp60,gdf$yearsschool,gdf$growth,color="grey",radius=0.15,
            specular="white")
axes3d(c('x','y','z'))
title3d('','','GDP1960','School','Growth')
grid3d(c("x", "y+", "z"))

aspect3d(x=1 ,y=96/128,z=1 )
par3d(windowRect = c(100,100, 3000+100, 2000+100))
text3d(gdf$rgdp60,gdf$yearsschool,gdf$growth,gdf$country_name, cex=3)

coefs <- greg$coefficients
#planes3d(a=lm(growth ~ rgdp60,data=gdf)$coef["rgdp60"],b=0,-1,
#         lm(growth ~ rgdp60,data=gdf)$coef["(Intercept)"],alpha=0.2,col="red")
#planes3d(a=0,b=lm(growth ~ yearsschool,data=gdf)$coef["yearsschool"],-1,
#         lm(growth ~ yearsschool,data=gdf)$coef["(Intercept)"],alpha=0.2,col="green")

planes3d(a=coefs["rgdp60"], b=coefs["yearsschool"], -1,
         coefs["(Intercept)"], alpha=0.20, col="plum2")

play3d(spin3d(axis=c(0,0,1),rpm=3)) ## for viewing
## save
#rgl.snapshot("growth.png",fmt="png",top=TRUE)




## create nice LaTeX table of regression results
require(texreg)
texTable <-  texreg(list(lm(growth ~ rgdp60, data=gdf),
                         lm(growth ~ yearsschool, data=gdf),
                         lm(growth ~ rgdp60 + yearsschool, data=gdf)),
                    model.names=c("GDP 1960", "Education 1960", "Both"),
                    use.packages=FALSE,
                    caption="Growth and GDP and education in 1960",
                    label="tab:growth", digits=3)
cat(texTable,file="growth.tex")

