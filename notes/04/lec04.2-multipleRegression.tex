\title{Properties of OLS in the multiple regression model}
\author{Paul Schrimpf} 
\institute{UBC \\ Economics 326} 
\date{\today}

\graphicspath{{figures/}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item \cite{w2013} chapters 3 and 4
  \item \cite{sw2009} chapter 7 and 18
  \item \cite{ap2014} chapter 2        
  \item
    \href{http://faculty.arts.ubc.ca/hkasahara/Econ326/326_10_mreg_properties_handout.pdf}
    {Kasahara's slides}
  \item \cite{bierens2010mreg}
  \item \cite{ap2009} pages 48-69
  \item \cite{baltagi2002} chapter 4
  \end{itemize}
\end{frame}

\section{Setup}

These notes discuss the properties of OLS in the multiple regression
model. The assumptions and results are nearly identical to what we
showed for simple bivariate regression. As in bivariate regression, we
will begin by making the following four assumptions to show that OLS
is unbiased.

\begin{frame}\frametitle{Model}
  \begin{align}
    y_i = \beta_0 + \beta_1 x_{1,i} + \cdots  + \beta_k x_{k,i} + \epsilon_i \label{mr1}
  \end{align}
  Assumptions:
  \begin{enumerate}
  \item[MLR.1] (linear model) equation \ref{mr1} holds
  \item[MLR.2] (independence) $\{(x_{1,i},x_{2,i},y_i)\}_{i=1}^n$ is
    an independent random sample
  \item[MLR.3] (rank condition) no multicollinearity: no $x_{j,i}$ is
    constant and there is no exact linear relationship among the $x_{j,i}$
  \item[MLR.4] (exogeneity) $\Er[\epsilon_i | x_{1,i},..., x_{k,i}] = 0$
  \end{enumerate}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Discussion of assumptions}
  \begin{itemize}
  \item Assumptions MLR.1 (linear model), MLR.2 (independence), and MLR.4
    (exogeneity) are the same as in bivariate regression
  \item MLR.1 (linear model)
    \begin{itemize}
    \item Only by writing down a model can we talk about bias and
      exogeneity
    \item Linearity is a convenient approximation
    \end{itemize}
  \item MLR.2 (independence)
    \begin{itemize}
    \item OLS generally still unbiased with non-independent
      observations, but variance different
    \item Forms of dependence: time series, clustering, spatial 
    \end{itemize}
  \item MLR.4 (exogeneity) 
    \begin{itemize}
    \item Key assumption for OLS to be unbiased
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Discussion of assumpptions}
  \begin{itemize}
  \item MLR.3 (rank condition / no perfect collinearity): Wooldridge
    ``None of the independent variables ($x$'s) is constant, and there
    are no exact linear relationships among the independent variables''
    \begin{itemize}
    \item Ensures that there is are unique values for $\hat{\beta}_0$,
      ..., $\hat{\beta}_k$ that solve the first order conditions,
      \begin{align*}
        \sumin & \left(y_i - \hat{\beta}_0 - \hat{\beta}_1 x_{1,i} -
          \cdots - \hat{\beta}_k x_{k,i} \right) & & = 0 \\
        \sumin & \left(y_i - \hat{\beta}_0 - \hat{\beta}_1 x_{1,i} -
          \cdots - \hat{\beta}_k x_{k,i} \right) &x_{1,i} & = 0 \\
        \vdots & & \vdots  & =  \vdots \\
        \sumin & \left(y_i - \hat{\beta}_0 - \hat{\beta}_1 x_{1,i} -
          \cdots - \hat{\beta}_k x_{k,i} \right) &x_{k,i} & = 0 
      \end{align*}
    \item In the language of linear algebra\footnote{If you have not
        heard of matrices and their rank before, this bullet point can
        safely be ignored.}, this condition says that rank of the
      following $n \times (k+1)$ matrix of $x$'s,
      \[ \begin{pmatrix} 1 & x_{1,1} & \cdots & x_{k,1} \\
        1 & x_{1,2} & \cdots & x_{k,2} \\
        \cdots &    &        & \vdots \\
        1 & x_{1,n} & \cdots & x_{k,n} 
      \end{pmatrix} \]
      must be $k+1$      
    \end{itemize}          
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{No collinearity}
  \begin{center}
    \includegraphics[height=0.7\textheight]{notCollinear010} 
  \end{center}
  Cloud of points, so there's a unique plane that minimizes squared
  residuals
  \footnotetext{
    \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/04/collinear.R?at=master}
    {Code}}
\end{frame}

\begin{frame}\frametitle{Perfect collinearity}
  \begin{center}
    \includegraphics[height=0.7\textheight]{collinear010} 
  \end{center}
  All points lie in one plane, so there's many planes that minimize
  squared residuals
  \footnotetext{
    \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/04/collinear.R?at=master}
    {Code}}
\end{frame}

\section{Expected value}

\begin{frame}[allowframebreaks]\frametitle{OLS is unbiased}
  \begin{theorem}
    Under assumptions MLR.1-4, OLS is unbiased,
    \[ \Er[\hat{\beta}_j] = \beta_j \text{ for } j=0,1,...,k. \]    
  \end{theorem}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Proof that OLS is unbiased}
  \begin{itemize}
  \item Use partitioned regression formula,
    \begin{align} 
      \hat{\beta}_1 = \frac{\sumin \tilde{x}_{1,i} y_i}{\sumin
        \tilde{x}_{1,i}^2} \label{partreg}
    \end{align}
    where $\tilde{x}_{1,i}$ is the OLS  residual from regressing
    $x_{1,i}$ on the other controls,
    \[ x_{1,i} = \hat{\gamma}_0 + \hat{\gamma}_2 x_{2,i} + \cdots +
    \hat{\gamma}_k x_{k,i} + \tilde{x}_{1,i} \]
  \item Substitute $y_i = \beta_0 + \beta_1 x_{1,i} + \cdots  +
    \beta_k x_{k,i} + \epsilon_i$ in (\ref{partreg})
    \begin{align} 
      \hat{\beta}_1 = & \frac{\sumin \tilde{x}_{1,i} \left( \beta_0
          + \beta_1 x_{1,i} + \cdots + \beta_k x_{k,i} + \epsilon_i
        \right)}{\sumin \tilde{x}_{1,i}^2} \notag
    \end{align}
  \item Rearrange and use the following properties of residuals
    $\tilde{x}_{1,i}$ to simplify
    \begin{enumerate}
    \item $\sumin \tilde{x}_{1,i} = 0 $
    \item $\sumin \tilde{x}_{1,i} x_{j,i} = 0$ for $j=2,...,k$
    \item $\sumin \tilde{x}_{1,i} x_{1,i} = \sumin \tilde{x}_{1,i}^2$
    \end{enumerate}
    and get
    \begin{align} 
      \hat{\beta}_1 = & \beta_1 + \frac{\sumin \tilde{x}_{1,i}
        \epsilon_i}{\sumin \tilde{x}_{1,i}^2} \label{hatbeta1}
    \end{align}
  \item Take expectations of (\ref{hatbeta1}) and use iterated
    expectations
    \begin{align*}
      \Er[\hat{\beta}_1] = & \Er\left[\Er[\hat{\beta}_1 | X] \right]\\
      = & \beta_1 + \Er\left[ \Er\left[\left. \frac{\sumin \tilde{x}_{1,i}
            \epsilon_i}{\sumin \tilde{x}_{1,i}^2} \right\vert X \right]
      \right]
    \end{align*}
  \item Use MLR.4 to conclude
    \[ \Er[\hat{\beta}_1] = \beta_1 \]
  \item Identical argument works for $j=2,...,k$
  \end{itemize}
\end{frame}

\section{Variance}

\begin{frame}\frametitle{Variance}
  \begin{itemize}
  \item[MLR.5] (homoskedasticity) $\var(\epsilon_i | X) =
    \sigma^2_\epsilon$
  \end{itemize}
  \begin{theorem}
    Under assumptions MLR.1-5, the variance of OLS conditional on the
    controls is
    \[ \var(\hat{\beta}_j|X) = \frac{\sigma_\epsilon^2}{\sumin
      \tilde{x}_{j,i}^2}. \]
    Also, the covariance of $\hat{\beta}_j$ and $\hat{\beta}_\ell$
    conditional on the controls is
    \[ \cov(\hat{\beta}_j,\hat{\beta}_\ell|X) = \sigma_\epsilon^2
    \frac{\sumin \tilde{x}_{j,i}\tilde{x}_{\ell,i}} 
    { \left(\sumin \tilde{x}_{j,i}^2\right) \left(\sumin
        \tilde{x}_{\ell,i}^2\right) }
    \]    
  \end{theorem}  
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Proof of OLS variance}
  \begin{itemize}
  \item Use 
    \[ \hat{\beta}_j = \beta_j + \frac{\sumin \tilde{x}_{j,i}
      \epsilon_i}{\sumin \tilde{x}_{j,i}^2} \]
  \item So
    \[ \hat{\beta}_j - \Er[\hat{\beta}_j] =  \hat{\beta}_j - \beta_j =
    \frac{\sumin \tilde{x}_{j,i} \epsilon_i}{\sumin
      \tilde{x}_{j,i}^2} \] 
  \item Then,
    \begin{align*}
      \cov(\hat{\beta}_j,\hat{\beta}_\ell) = & 
      \Er\left[ (\hat{\beta}_j - \Er[\hat{\beta}_j]) (\hat{\beta}_\ell
        - \Er[\hat{\beta}_\ell]) | X \right] \\
      = & \Er\left[\left. \frac{\sumin \tilde{x}_{j,i} \epsilon_i}{\sumin
            \tilde{x}_{j,i}^2} 
          \frac{\sumin \tilde{x}_{\ell,i} \epsilon_i}{\sumin
            \tilde{x}_{\ell,i}^2} \right\vert X \right]
    \end{align*}
  \item Use MLR.5 to get desired result
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Alternative expression for the variance}
  \begin{itemize}
  \item From above we have
    \[ \var(\hat{\beta}_j|X) = \frac{\sigma_\epsilon^2}{\sumin
      \tilde{x}_{j,i}^2}. \]
    and
    \[ \cov(\hat{\beta}_j,\hat{\beta}_\ell|X) = \sigma_\epsilon^2
    \frac{\sumin \tilde{x}_{j,i}\tilde{x}_{\ell,i}} 
    { \left(\sumin \tilde{x}_{j,i}^2\right) \left(\sumin
        \tilde{x}_{\ell,i}^2\right) }
    \]  
  \item The dominator, $\sumin \tilde{x}_{j,i}^2$, is the sum of
    squared residuals from regression $x_{j,i}$ on the other $x$'s
  \item Recall $R^2 = \frac{SSE}{SST} = 1 - \frac{SSR}{SST}$, so
    \begin{align*}
      R^2_j = & 1 - \frac{ \sumin \tilde{x}_{j,i}^2}{\sumin (x_{j,i} -
      \bar{x}_j)^2}  \\
      \sumin \tilde{x}_{j,i}^2 = & (1 - R^2_j) \sumin (x_{j,i} - 
                                   \bar{x}_j)^2 \\
      = & (1-R^2_j) n \widehat{\var}(x_j)
    \end{align*}
  \item So,
    \[ \var(\hat{\beta}_j|X) = \frac{\sigma_\epsilon^2}{\sumin
      \tilde{x}_{j,i}^2} = \frac{\sigma_\epsilon^2} {(1-R^2_j) n
      \widehat{\var}(x_j)} \] 
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Variance and omitted variables}
  \begin{itemize}
  \item Suppose 
    \[ y_i = \beta_0 + \beta_1 x_{1,i} + \beta_2 x_{2,i} +
      \epsilon_i \]
    and MLR1-5 hold
  \item What are the bias of $\hat{\beta}_1^s$ and $\hat{\beta}_1$?
  \item What are $\var(\hat{\beta}_1^s)$ and $\var(\hat{\beta}_1)$?
    Which is larger?
  \item What is the mean square error (MSE) of $\hat{\beta}_1^s$ and $\hat{\beta}_1$?
  \item If we want to minimize MSE, what is better $\hat{\beta}_1^s$
    or $\hat{\beta}_1$?
  \end{itemize}
\end{frame}

\section{Efficiency}

\begin{frame}\frametitle{Gauss-Markov theorem}
  \begin{theorem}
    Under assumptions MLR.1-5, OLS is the best linear unbiased
    estimator
  \end{theorem}
  \begin{itemize}
  \item By best linear unbiased estimator, we mean that OLS has the
    lowest variance for any linear combination of the coefficients,
    \[ \var(\sum_{j=1}^k \lambda_j \hat{\beta}_j) \leq
    \var(\sum_{j=1}^k \lambda_j \tilde{\beta}_j)  \]
    where $\tilde{\beta}_j$ are any other linear unbiased estimators
    \begin{itemize}
    \item Linear: $\tilde{\beta}_j = \sumin w_i y_i $
    \item Unbiased: $\Er[\tilde{\beta}_j] = \beta_j$
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Distribution}

\begin{frame}\frametitle{Distribution}
  \begin{itemize}
  \item[MLR.6] $\epsilon_i |X \sim N(0,\sigma_\epsilon^2)$
  \end{itemize}
  \begin{theorem}
    Under assumptions MLR.1-6, OLS is normally distributed
    {\footnotesize{
    \[ \begin{pmatrix} 
      \hat{\beta}_0 \\ \hat{\beta}_1 \\ \vdots \\ \hat{\beta}_k 
    \end{pmatrix} |X \sim N 
    \left(
      \begin{pmatrix}
        \beta_0 \\ \beta_1 \\ \vdots \\ \beta_k 
      \end{pmatrix},
      \begin{pmatrix}
        \var(\hat{\beta}_0|X) &  \cov(\hat{\beta}_0,\hat{\beta}_1|X) &
        \cdots &  \cov(\hat{\beta}_0,\hat{\beta}_k|X)  \\ 
        \cov(\hat{\beta}_1,\hat{\beta}_0|X) & \var(\hat{\beta}_1|X) &
        \cdots &  \cov(\hat{\beta}_1,\hat{\beta}_k|X)  \\  
        \vdots & \vdots &\ddots & \vdots \\
        \cov(\hat{\beta}_k,\hat{\beta}_0|X) & \cov(\hat{\beta}_k,\hat{\beta}_1|X) &
        \cdots &  \var(\hat{\beta}_k|X)               
      \end{pmatrix}
    \right)
    \]
  }}
  \end{theorem}
\end{frame}

\begin{frame} \frametitle{Standard bivariate normal density}
  \includegraphics[width=\textwidth,height=0.9\textheight]{mnorm} 
\end{frame}

\begin{frame} \frametitle{Bivariate normal density with correlation}
  \includegraphics[width=\textwidth,height=0.9\textheight]{mnorm-corr} 
\end{frame}


\section{Inference}

\begin{frame}[allowframebreaks]
  \frametitle{Inference}
  \frametitle{Inference with normal errors}
  \begin{itemize}
  \item Regression estimates depend on samples, which are random, so
    the regression estimates are random
    \begin{itemize}
    \item Some regressions will randomly look ``interesting'' due to
      chance 
    \end{itemize}
  \item Logic of hypothesis testing: figure out probability of getting
    an interesting regression estimate due solely to change
  \item Null hypothesis, $H_0:$ the regression is uninteresting
    \begin{itemize}
    \item If we mainly care about the $j$th control, then 
      \[ H_0 : \beta_j = 0 \]
    \item If we care about all the regressors, then maybe
      \[ H_0: \beta_1 = 0 , \beta_2 = 0, ..., \beta_k = 0 \]      
    \end{itemize}
  \item With assumptions MLR.1-MLR.6 and under $H_0: \beta_j =
    \beta_j^\ast$, we know
    \[ \hat{\beta}_j \sim N\left(\beta_j^\ast,
      \frac{\sigma^2_\epsilon}{\sum_{i=1}^n \tilde{x}_{j,i}^2} 
    \right) \]
    or equivalently,
    \[ z \equiv \frac{\hat{\beta}_j - \beta_j^\ast}{\sigma_\epsilon /
      \sqrt{\sum_{i=1}^n \tilde{x}_{j,i}^2} } \sim N(0,1) \]
  \item We do not know $\sigma^2_\epsilon$, so estimate it using
    residuals,
    \[ \hat{\sigma}_\epsilon^2 = \frac{1}{n-(k+1)} \sumin
    \hat{\epsilon}_i^2 \]
    \begin{itemize}
    \item We divided by $n-(k+1)$ instead of $n$ because
      $\hat{\epsilon}_i$ depends on $k+1$ estimated parameters
      ($\hat{\beta}_0, \hat{\beta}_1$, ..., $\hat{\beta}_k$)
    \end{itemize}
  \item Because of estimated $\hat{\sigma}_\epsilon^2$, test statistic
    has $t$ distribution instead of normal,
    \[ t \equiv \frac{\hat{\beta}_j - \beta_j^\ast}{\hat{\sigma}_\epsilon /
      \sqrt{\sum_{i=1}^n \tilde{x}_{j,i}^2} } \sim t\left(n-(k+1)
    \right) \]     
  \item P-value: the probability of getting a regression estimate as
    or more ``interesting'' than the one we have
    \begin{itemize}
    \item As or more interesting $=$ as far or further away from
      $\beta_j^\ast$
    \item If we are only interested when $\hat{\beta}_j$ is on one
      side of $\beta_j^\ast$, then we have a one sided alternative,
      e.g.\ $H_a: \beta_j > \beta_j^\ast$
    \item If we are equally interested in either direction, then $H_a:
      \beta_j \neq \beta_j^\ast$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} 
  \includegraphics[width=\textwidth,height=0.9\textheight]{oneSided} 
\end{frame}

\begin{frame} 
  \includegraphics[width=\textwidth,height=0.9\textheight]{twoSided} 
\end{frame}
  
\begin{frame}[allowframebreaks]
  \frametitle{Inference with normal errors}
  \begin{itemize}
  \item One-sided p-value: $p = F_{t,n-k-1}(-\abs{t}) = 1-F_{t,n-k-1}(\abs{t})$ \\
  \item Two-sided p-value: $p = 2F_{t,n-k-1}(-\abs{t}) = 2(1-F_{t,n-k-1}(\abs{t}))$
  \item Interpretation:
    \begin{itemize}
    \item The probability of getting an estimate as strange as the one
      we have if the null hypothesis is true.
    \item It is \emph{not} about the probability of $\beta_j$ being
      any particular value. $\beta_j$ is not a random variable. It is
      some unknown number. The data is what is random. In particular,
      the p-value is \emph{not} the probability that that $H_0$ is false
      given the data.
    \end{itemize}
  \item Hypothesis testing: we must make a decision (usually reject or
    fail to reject $H_0$)
    \begin{itemize}
    \item Choose significance level $\alpha$ (usually 0.05 or 0.10)
    \item Construct procedure such that if $H_0$ is true, we will
      incorrectly reject with probability $\alpha$
    \item Reject null if p-value less than $\alpha$
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Example: growth, GDP, and schooling}

\begin{frame}\frametitle{Example: growth, GDP, and schooling}
  \includegraphics[width=\textwidth,height=0.9\textheight,keepaspectratio=TRUE]{growth}
  \\
  \footnotetext{
    \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/04/growth-mr.R?at=master}
    {Code}}
\end{frame}

\begin{frame}\frametitle{Example: growth, GDP, and schooling}
  \input{figures/growth}
\end{frame}

\subsection{Confidence intervals}

\begin{frame}[allowframebreaks]
  \frametitle{Confidence intervals}
  \begin{itemize}
  \item A $1-\alpha$ confidence interval, $CI_{1-\alpha} =
    [LB_{1-\alpha}, UB_{1-\alpha}]$ is an interval estimator for
    $\beta_j$ such that 
    \[ \Pr\left(\beta_j \in CI_{1-\alpha}\right) = 1-\alpha \]
    ($CI_{1-\alpha}$ is random; $\beta_j$ is not)
  \item $1-\alpha$ confidence interval
    \[ \hat{\beta}_j \pm \sqrt{\var(\hat{\beta}_j)}
    \Phi^{-1}(\alpha/2) \]
  \item With estimated $\hat{\sigma}_\epsilon^2$, use t distribution
    instead of normal
    \[ \hat{\beta}_j \pm \sqrt{\widehat{\var}(\hat{\beta}_j)}
    F_{t,n-2}^{-1}(\alpha/2) \]
    $F_{t,n-2}^{-1} = $ inverse CDF of $t(n-2)$ distribution 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Confidence intervals}
  \includegraphics[width=\textwidth,height=0.9\textheight]{ci}
\end{frame}

\subsection{Example: \cite{kearney2012}}

\begin{frame}\frametitle{Example: \cite{kearney2012}}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{kl1}
\end{frame}

\begin{frame}\frametitle{Example: \cite{kearney2012}}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{kl2}
\end{frame}

\begin{frame}\frametitle{Example: \cite{kearney2012}}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{kl2a}
\end{frame}

\begin{frame}\frametitle{Example: \cite{kearney2012}}
  \begin{center}
    \includegraphics[keepaspectratio=true,width=\textwidth,height=0.8\textheight]{kl-tab2}
  \end{center}
\end{frame}

\begin{frame}\frametitle{Example: \cite{kearney2012}}
  \begin{center}
    \includegraphics[keepaspectratio=true,height=0.8\textheight]{kl-fig4}
  \end{center}
\end{frame}

\begin{frame}\frametitle{Example: \cite{kearney2012}}
  \begin{center}
    \includegraphics[keepaspectratio=true,height=0.8\textheight]{kl3}
  \end{center}
\end{frame}

\subsection{Testing hypotheses involving multiple coefficients}

\begin{frame}\frametitle{Hypothesis tests for multiple coefficients}
  \begin{itemize}
  \item Sometimes we want to test a hypothesis that involves multiple
    coefficients
    \begin{enumerate}
    \item Single restriction on a linear combinations of coefficients, e.g.\ 
      \[ H_0 : \beta_1 = \beta_2 \]
      same as 
      \[ H_0 : \beta_1 - \beta_2 = 0 \]
    \item Multiple restrictions, e.g.\ 
      \[ H_0: \beta_1 = 0 \text{ and } \beta_2 = 0 \]
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Hypothesis tests for linear combination of
    coefficients}
  \begin{itemize}
  \item $H_0: \lambda_1 \beta_1 + \cdots + \lambda_k \beta_k = 0$
  \item To simplify notation, focus on $H_0: \beta_1 - \beta_2 = 0$
  \item We know that 
    \[ \begin{pmatrix} \hat{\beta}_1 \\ \hat{\beta}_2
    \end{pmatrix} \sim N\left( \begin{pmatrix} \beta_1 \\ \beta_2 
      \end{pmatrix}, 
      \begin{pmatrix} \var(\hat{\beta}_1) &
        \cov(\hat{\beta}_1,\hat{\beta}_2) \\
        \cov(\hat{\beta}_1,\hat{\beta}_2)  & \var(\hat{\beta}_2) 
      \end{pmatrix} \right)
    \]
    so 
    \begin{align*}
      \hat{\beta}_1 - \hat{\beta}_2 \sim N\left( \beta_1 - \beta_2, 
        \var(\hat{\beta}_1) + \var(\hat{\beta}_2) - 2
        \cov(\hat{\beta}_1,\hat{\beta}_2) \right)
    \end{align*}
    and under $H_0: \beta_1 = \beta_2$, 
    \[ t \equiv \frac{\hat{\beta}_1 - \hat{\beta}_2} 
    {\sqrt{\widehat{\var}(\hat{\beta}_1) +
        \widehat{\var}(\hat{\beta}_2) - 2
        \widehat{\cov}(\hat{\beta}_1,\hat{\beta}_2) } } \sim
    t(n-k-1) \]     
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Hypothesis tests for linear combination of
    coefficients}
  \begin{itemize}
  \item Example: \cite{kearney2012} test $H_0: \beta_{sex} +
    \beta_{protection} = 0$
    \begin{itemize}
    \item 
      \begin{align*} t = & \frac{\hat{\beta}_{sex} + \hat{\beta}_{protection}} 
      {\sqrt{\widehat{\var}(\hat{\beta}_1) +
          \widehat{\var}(\hat{\beta}_2) + 2
          \widehat{\cov}(\hat{\beta}_1,\hat{\beta}_2) } } \\ = &
      \frac{0.162 + -0.186}{\sqrt{0.017^2 + 0.03^2 + \cov}} 
      \end{align*}
    \item $\cov$ not reported, but we can consider the possible range
      \begin{itemize}
      \item $\cov=0$: $t = \frac{-0.024}{0.034} = 0.68$
      \item
        $\cov=+\sqrt{\widehat{\var}(\hat{\beta}_1)\widehat{\var}(\hat{\beta}_2)}$:
        $t = \frac{-0.024}{0.047} = 0.51$
      \item $\cov=-\sqrt{\widehat{\var}(\hat{\beta}_1)\widehat{\var}(\hat{\beta}_2)}$:
        $t = \frac{-0.024}{0.013} = 1.85$
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Hypothesis tests for linear combination of
    coefficients}
  \begin{itemize}
  \item Another approach to testing linear combinations of
    coefficients is to re-specify the model so that the null hypothesis
    is about a single coefficient
    \begin{itemize}
    \item E.g.\ instead of 
      \[ y_i = \beta_0 + \beta_1 x_{1,i} + \beta_2 x_{2,i} +
      \epsilon_i \]
      with $H_0: \beta_1 - \beta_2 = 0$, write 
      \begin{align*}
        y_i = & \beta_0 + \underbrace{\beta_1}_{=\theta_1} (x_{1,i} + x_{2,i}) + \underbrace{(\beta_2 -
          \beta_1)}_{=\theta_2} x_{2,i} + \epsilon_i \\ 
        = & \theta_0 + \theta_1 (x_{1,i} + x_{2,i}) + \theta_2 x_{2,i}
        + \epsilon_i 
      \end{align*}
      then test $H_0: \theta_2 = 0$. 
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]\frametitle{Testing multiple restrictions}
  \begin{itemize}
  \item What if we have 
    \[ y_i = \beta_0 + \beta_1 x_{1,i} + \beta_2 x_{2,i} +
    \epsilon_i \]
    and want to test $H_0: \beta_1 = 0 $ and $\beta_2 = 0$?
  \item Cannot test one at time:
    \begin{itemize}
    \item Suppose test $H_0: \beta_1 = 0$ and $H_0':\beta_2 = 0$
      separately at $5\%$ significance level
    \item If $H_0$ true and $\cov(\hat{\beta}_1,\hat{\beta}_2) = 0$,
      then $\Pr(\text{fail to reject both}) = 0.95 \times 0.95 =
      0.9025$, so doing separate tests we will reject too often  
    \end{itemize}
  \item Can use joint normal distribution of $\hat{\beta_1}$ and
    $\hat{\beta}_2$
    \begin{itemize}
    \item p-value $= \Pr(\text{estimates as far or further from null
        hypothesis})$ 
    \item Need to take into account correlation of $\hat{\beta}_1$ and
      $\hat{\beta}_2$  
    \end{itemize}
  \item $F$-statistic 
    \begin{align*}
      F = & \frac{1}{2} \begin{pmatrix} \hat{\beta}_1 \\ \hat{\beta}_2
      \end{pmatrix}^T \begin{pmatrix} \widehat{\var}(\hat{\beta}_1)
        & \widehat{\cov}(\hat{\beta}_1, \hat{\beta}_2) \\
        \widehat{\cov}(\hat{\beta}_1, \hat{\beta}_2) &
        \widehat{\var}(\hat{\beta}_2) 
      \end{pmatrix}^{-1} \begin{pmatrix} \hat{\beta}_1 \\ \hat{\beta}_2
      \end{pmatrix} \\
      = & \frac{1}{2} \frac{\hat{\beta}_1^2 \widehat{\var}(\hat{\beta}_2) +
        \hat{\beta}_2^2 \widehat{\var}(\hat{\beta}_1) - 2
        \widehat{\cov}(\hat{\beta}_1,\hat{\beta}_2) \hat{\beta}_1
        \hat{\beta}_2 } {\widehat{\var}(\hat{\beta}_1)
        \widehat{\var}(\hat{\beta}_2) -
        \widehat{\cov}(\hat{\beta}_1,\hat{\beta}_2)^2} 
    \end{align*}
    has an $F(2, n-2)$ distribution
    \begin{itemize}
    \item $k F(k,\infty) = \chi^2(k) = $ distribution of the sum of $k$
      independent standard normal random variables each squared
    \end{itemize}
  \item This $F$ is valid even with heteroskedasticity and
    dependent observations as long as the variances are calculated
    correctly 
  \end{itemize} 
\end{frame}

\begin{frame} \frametitle{$F(2,n-2)$ distributions}
  \includegraphics[width=\textwidth,height=0.9\textheight]{F-df}
\end{frame}

\begin{frame} \frametitle{Standard bivariate normal density}
  \includegraphics[width=\textwidth,height=0.9\textheight]{mnorm-crit} 
\end{frame}

\begin{frame} \frametitle{$F(2,\infty)$ distribution}
  \includegraphics[width=\textwidth,height=0.9\textheight]{F-crit}
\end{frame} 

\begin{frame} \frametitle{Bivariate normal density with correlation}
  \includegraphics[width=\textwidth,height=0.9\textheight]{mnorm-corr-crit} 
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Testing multiple
    restrictions: alternative form of $F$ statistic}
  \begin{itemize}
  \item We have an unrestricted model:
    \[ y_i = \beta_0 + \beta_1 x_{1,i} + \cdots  + \beta_k x_{k,i} +
    \epsilon_i \]
    and a restricted model that imposes $H_0: \beta_{k-q+1} = 0, ...,
    \beta_k = 0$
  \item Estimate both models, compute sum of squared residuals call
    $SSR_{ur}$ and $SSR_r$
  \item Calculate $F$ statistics
    \[ F \equiv \frac{(SSR_r - SSR_{ur})/q}{SSR_{ur}/(n-k-1)} \]
  \item $F \sim F(q,n-k-1)$, use to calculate p-values
  \item This form of $F$ is only valid with homoskedasticity and
    independence 
  \end{itemize}
\end{frame}

\subsection{Example: \cite{aef2013}}

\begin{frame}\frametitle{\cite{aef2013}}
  \includegraphics[keepaspectratio=true,width=1\textwidth]{aef1}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
\bibliographystyle{jpe}
\bibliography{../326}
\end{frame}
  

\end{document}