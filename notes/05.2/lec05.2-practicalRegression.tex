\title{Qualitative Variables \& Functional Form}
\author{Paul Schrimpf} 
\institute{UBC \\ Economics 326} 
\date{\today}

\graphicspath{{figures/}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item \cite{w2013} chapters 6 \& 7
  \item \cite{sw2009} chapter 8
  \item \cite{ap2014} chapter 2, especially section 2.2
  \end{itemize}
\end{frame}           

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Functional form}

\begin{frame}\frametitle{Functional form}
  \begin{itemize}
  \item We can use functions of variables in regression models
  \item Example: wages, years of education, \& experience
    \begin{itemize}
    \item[a] $Wage_{i}=\beta^{(a)} _{0}+ \beta^{(a)}_{1} Educ_{i} + \beta^{(a)}_2 Exper_i + 
      \epsilon_{i}$
    \item[b] $\log Wage_{i}=\beta^{(b)} _{0}+ \beta^{(b)}_{1} Educ_{i} + \beta^{(b)}_2
      Exper_i + \epsilon_{i}$
    \item[c] $\log Wage_{i}=\beta^{(c)} _{0}+ \beta^{(c)}_{1} Educ_{i} + \beta^{(c)}_2
      Exper_i + \beta^{(c)}_3 (Exper_i)^2
      \epsilon_{i}$
    \end{itemize}
  \item All three models can be estimated by OLS
  \item What is the interpretation of $\beta^{(a)}$ vs $\beta^{(b)}$
    vs $\beta^{(c)}$?
  \item How to choose which model to estimate?
  \end{itemize}
\end{frame}

\subsection{Logarithms}

\begin{frame}\frametitle{Logarithms}
  \begin{itemize}
  \item We often estimate regressions using logarithms of some
    variables instead of the level
  \item Example: wages and years of education
    \begin{itemize}     
    \item Level: $Wage_{i}=\beta^{lev} _{0}+ \beta^{lev}
      _{1} Educ_{i} + \epsilon_{i}$
    \item Logarithm: $\log(Wage_{i}) =\beta^{\log}_{0}+ \beta^{\log}
      _{1} Educ_{i} + \epsilon_{i}^{\log}$
    \end{itemize}
  \item Interpretation of slope
    \begin{itemize}
    \item Each additional year of education is associated with
      an increase in wages of $\beta^{lev}$ dollars
    \item Each additional year of education is associated with an
      increase in \textbf{log} wages of $\beta^{\log}$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Change in logarithm approximates percent change}
  \begin{itemize}
  \item If $\log w$ changes by $\Delta$, then change the percent
    change in $w$ is
    \begin{align*}
      100 \frac{w_{new} - w_{old}}{w_{old}}
      = & 100 \frac{e^{(\log w)  + \Delta} - e^{\log w}}{e^{\log w}} \\
      = & 100 \frac{e^{\log w}}{e^{\log w}} (e^\Delta - 1) \\
      = & 100 \left(\Delta + \Delta^2/2 + \Delta^3/3! + \Delta^4/4! + \cdots \right)  &
                                                                       \text{
                                                                       Taylor
                                                                       expansion}
      \\
      \approx & 100 \Delta
    \end{align*}
  \item So $100 \beta^{\log}_1$ is the percentage change in wages
    associated with an additional year of education
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Change in logarithm approximates percent change}
  \begin{itemize}
  \item In,
    \[ \log(Wage_{i}) =\beta^{\log:\log}_{0}+ \beta^{\log:\log}
      _{1} \log(Educ_{i}) + \epsilon_{i}^{\log:\log} \]
    \begin{itemize}
    \item $\beta_1^{\log:\log}$ is the change in $\log$ wage associated with a change in
      $\log$ years of education
    \item A 1\% change in $Educ$ increases $\log(Educ)$ by 0.01, so
      $\log(Wage)$ increases by $0.01 \beta_1^{\log:\log}$, or $Wage$
      increases by $\beta_{1}^{\log:\log}$ \alert{\%}
    \item $\beta_1^{\log:\log}$ is the elasticity of wages with
      respect to education
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Logs vs levels}
  \begin{itemize}
  \item When to use logarithms instead of levels?
  \item No universal rules
  \item Logarithms useful when:
    \begin{itemize}
    \item Percent changes are desirable
    \item Variable in levels is positive and right skewed e.g.\
      income, population, wealth
    \end{itemize}
  \item Logarithm cannot be used with variables that are $\leq 0$
  \end{itemize}
\end{frame}


\subsection{Polynomials}

\begin{frame}[allowframebreaks] \frametitle{Polynomials}
  \begin{itemize}
  \item Example: wages, years of education, \& experience
    \begin{itemize}
    \item[b] $\log Wage_{i}=\beta^{(b)} _{0}+ \beta^{(b)}_{1} Educ_{i} + \beta^{(b)}_2
      Exper_i + \epsilon_{i}$
    \item[c] $\log Wage_{i}=\beta^{(c)} _{0}+ \beta^{(c)}_{1} Educ_{i} + \beta^{(c)}_2
      Exper_i + \beta^{(c)}_3 (Exper_i)^2
      \epsilon_{i}$
    \end{itemize}
  \item Interpretation:
    \begin{itemize}
    \item $\beta_2^{(b)}$ is the increase in $\log Wage$ associated
      with an additional year of experience
    \item $\beta_2^{(c)}$ is the increase in $\log Wage$ associated
      with an additional year of experience \alert{holding $Exper^2$
        fixed} -- does not make sense
    \item $\beta_2^{(c)} + 2 \beta_3^{(c)} \overline{Exper}$ is the
      average derivative of $\log Wage$ with respect to experience
    \end{itemize}
  \item When to use polynomials?
    \begin{itemize}
    \item Benefit: allows richer non-linear relationship 
    \item Costs: more difficult to report, more parameters to estimate
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Qualitative Variables}

\begin{frame}\frametitle{Qualitative Variables}

  \begin{itemize}
  \item  \alert{Categorical}
    variable is one that has one or more categories, but there is no natural
    ordering to the categories
    \begin{itemize}
    \item Examples: Gender, race, marital status, geographic location.
    \end{itemize}
  \item The following two variables are equivalent:
    
    $$Gender_{i}=\left\{ 
      \begin{array}{ll}
        1 & \text{if observation }i\text{ corresponds to a \textit{woman},} \\ 
        2 & \text{if observation }i\text{ corresponds to a\textit{\ man}.}
      \end{array}
    \right. $$
    
    $$Gender_{i}=\left\{ 
      \begin{array}{ll}
        1 & \text{if observation }i\text{ corresponds to a \textit{man},} \\ 
        2 & \text{if observation }i\text{ corresponds to a \textit{woman}.}%
      \end{array}%
    \right. $$
  \item Qualitative variables cannot be simply included in regression, because
    their units are arbitrary
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Dummy variables}
  \begin{itemize}
  \item A 
    \alert{dummy}
    variable is a binary zero-one variable which takes on the value one if some
    condition is satisfied and zero if that condition fails
    \begin{itemize}
    \item $Female_{i}=\left\{ 
        \begin{array}{ll}
          1 & \text{if observation }i\text{ corresponds to a woman,} \\ 
          0 & \text{if observation }i\text{ corresponds to a man.}%
        \end{array}
      \right. $
    \item $Male_{i}=\left\{ 
        \begin{array}{ll}
          1 & \text{if observation }i\text{ corresponds to a man,} \\ 
          0 & \text{if observation }i\text{ corresponds to a woman.}%
        \end{array}%
      \right. $
    \item Note that $Female_{i}+Male_{i}=1$ for all observations $i$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[squeeze,shrink]\frametitle{A single dummy independent variable}
  \begin{itemize}
  \item Consider the following regression:%
    \begin{equation*}
      Wage_{i}=\beta _{0}+\delta _{0}Female_{i}+\beta _{1}Educ_{i}+\beta
      _{3}Exper_{i}+\beta _{4}Tenure_{i}+\epsilon_{i},
    \end{equation*}%
    assume $\Er[\epsilon_i | Female, Educ, Exper, Tenure] = 0$.
  \item If observation $i$ corresponds to a woman, 
    $Female_{i}=1$, and
    \begin{align*}
        \Er[ Wage_{i}| &
          \color{red}
          Female_{i}=1
          \color{black} 
          ,Educ_{i},Exper_{i},Tenure_{i}] = \\
      & =\beta _{0}+
        \color{red}
        \delta _{0}
        \color{black}
      +\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta _{4}Tenure_{i}.
    \end{align*}
  \item If observation $i$ corresponds to a man, 
    $Female_{i}=0$, and
    \begin{align*}
        \Er[ Wage_{i}| &
          \color{red}
          Female_{i}=0
          \color{black}
          ,Educ_{i},Exper_{i},Tenure_{i}] = \\
      & =\beta _{0} +\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta
        _{4}Tenure_{i}.
    \end{align*}
  \item Thus,
    \begin{align*}
      \delta _{0}=
      & \Er[ Wage_{i}|
        \color{red}
        Female_{i}=1
        \color{black}
        ,Educ_{i},Exper_{i},Tenure_{i}] - \\
      & -\Er[ Wage_{i}|
        \color{red}
        Female_{i}=0
        \color{black}
        ,Educ_{i},Exper_{i},Tenure_{i}] 
    \end{align*}
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{An intercept shift}
  \begin{itemize}
  \item The model:
    \begin{equation*}
      Wage_{i}=\beta _{0}+\delta _{0}Female_{i}+\beta _{1}Educ_{i}+\beta
      _{3}Exper_{i}+\beta _{4}Tenure_{i}+\epsilon_{i}
    \end{equation*}%
  \item For men ($Female_{i}=0$):, we can write the model as
    \begin{equation*}
      Wage_{i}^{
        \color{blue} 
        M
        \color{black} 
      }=\beta _{0}+\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta
      _{4}Tenure_{i}+\epsilon_{i}.
    \end{equation*}
  \item For women ($Female_{i}=1$):, we can write the model as
    \begin{equation*}
      Wage_{i}^{
        \color{red}
        F \color{black}
      }=\left( \beta _{0}+\color{red}
        \delta _{0}
        \color{black}
      \right) +\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta _{4}Tenure_{i}+\epsilon_{i}.
    \end{equation*}
  \item In this case, men are called the 
    \textbf{reference} (or base)
    group
  \item $\delta _{0}$ measures the difference relative to the reference group.
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{An intercept shift}
  \includegraphics[width=\textwidth]{w-fig71}
\end{frame}


\begin{frame}\frametitle{Example}
  \begin{itemize}
  \item Estimated equation:
    \begin{align*}
      \widehat{Wage}_{i}= & -%
      \begin{array}[t]{l}
        1.57 \\ 
        \left( 0.72\right)%
      \end{array}%
      -%
      \begin{array}[t]{l}
        1.81 \\ 
        \left( 0.26\right)%
      \end{array}%
      Female_{i}+%
      \begin{array}[t]{l}
        0.572 \\ 
        \left( 0.049\right)%
      \end{array}%
      Educ_{i}+ \\
      & +
      \begin{array}[t]{l}
        0.025 \\ 
        \left( 0.012\right)%
      \end{array}%
      Exper_{i}+%
      \begin{array}[t]{l}
        0.141 \\ 
        \left( 0.021\right)%
      \end{array}%
      Tenure_{i}
    \end{align*}
  \item The dependent variable is the wage per hour
  \item $\hat{\delta}_{0}=-1.81$ implies that a women earns \$1.81 less per
    hour than a man with the same level of education, experience, and tenure.
    (These are 1976 wages.)
  \item The difference is also statistically significant.
  \end{itemize}
\end{frame}

\begin{frame}[squeeze,shrink] \frametitle{Logarithmic dependent variable} 
  \begin{itemize}
  \item The model:%
    \begin{equation*}
      \log \left( Wage\right) =\beta _{0}+\delta _{0}Female+\beta _{1}Educ+\beta
      _{3}Exper+\beta _{4}Tenure+\epsilon.
    \end{equation*}
  \item In this case,
    \begin{eqnarray*}
      \delta _{0} &=&\log \left( Wage^{F}\right) -\log \left( Wage^{M}\right) \\
                  &=&\log \left( \frac{Wage^{F}}{Wage^{M}}\right) \\
                  &=&\log \left( \frac{Wage^{M}+\left(
                      Wage^{F}-Wage^{M}\right) } {Wage^{M}}  \right) \\
                  &=&\log \left( 1+\frac{Wage^{F}-Wage^{M}}{Wage^{M}}\right) \\
                  &\approx &\frac{Wage^{F}-Wage^{M}}{Wage^{M}}.
    \end{eqnarray*}
  \item When the dependent variable is in the log form, $\delta _{0}$ has a 
    \textbf{percentage}  interpretation
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Example}
  \begin{itemize}
  \item Estimated equation:
    \begin{eqnarray*}
      \widehat{\log \left( Wage_{i}\right) } &=&
                                                 \begin{array}[t]{l}
                                                   0.417 \\ 
                                                   \left( 0.099\right)%
                                                 \end{array}%
      -
      \begin{array}[t]{l}
        0.297 \\ 
        \left( 0.036\right)%
      \end{array}%
      Female_{i}+%
      \begin{array}[t]{l}
        0.080 \\ 
        \left( 0.007\right)%
      \end{array}%
      Educ_{i}+ \\
                                             &&+%
                                                \begin{array}[t]{l}
                                                  0.029 \\ 
                                                  \left( 0.005\right)%
                                                \end{array}%
      Exper_{i}-%
      \begin{array}[t]{l}
        0.00058 \\ 
        (0.00010)%
      \end{array}%
      Exper_{i}^{2}+ \\
                                             &&+%
                                                \begin{array}[t]{l}
                                                  0.032 \\ 
                                                  \left( 0.007\right)%
                                                \end{array}%
      Tenure_{i}-%
      \begin{array}[t]{l}
        0.00059 \\ 
        (0.00023)%
      \end{array}%
      Tenure_{i}^{2}.
    \end{eqnarray*}%
  \item $\hat{\delta}_{0}=-0.297$ implies that a woman in 1976 earned
    29.7\% less than a man with the same level of education,
    experience and tenure.
  \end{itemize}
\end{frame}

\begin{frame}[squeeze,shrink]\frametitle{Changing the reference group}
  \begin{itemize}
  \item Instead of%
    \begin{equation*}
      \log \left( Wage_{i}\right) =\beta _{0}+\delta _{0}Female_{i}+\beta
      _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta _{4}Tenure_{i}+\epsilon_{i}
    \end{equation*}%
    consider:%
    \begin{equation*}
      \log \left( Wage_{i}\right) =\theta _{0}+\gamma _{0}Male_{i}+\theta
      _{1}Educ_{i}+\theta _{3}Exper_{i}+\theta _{4}Tenure_{i}+\epsilon_{i}.
    \end{equation*}%
  \item Since $Male_{i}=1-Female_{i},$%
    \begin{eqnarray*}
      \log \left( Wage_{i}\right) &=&\theta _{0}+\gamma _{0}Male_{i}
                                      + \theta_{1}Educ_{i} +
                                      \theta_3 Exper_{i}+\theta_{4}
                                      Tenure_{i}+\epsilon_{i} \\ 
&=&\theta _{0}+\gamma _{0}\left( 1-Female_{i}\right) +\theta
_{1}Educ_{i}+\theta _{3}Exper_{i}+\theta _{4}Tenure_{i}+\epsilon_{i} \\
&=&\left( \theta _{0}+\gamma _{0}\right) -\gamma _{0}Female_{i}+\theta
_{1}Educ_{i}+\theta _{3}Exper_{i}+\theta _{4}Tenure_{i}+\epsilon_{i}
    \end{eqnarray*}%
  \item We conclude that $\delta _{0}=-\gamma _{0},$ $\beta _{0}=\theta
    _{0}-\delta _{0},$ $\beta _{1}=\theta _{1},$ and etc.%
    \begin{equation*}
      \log \left( Wage_{i}\right) =\left( \beta _{0}+\delta _{0}\right) -\delta
      _{0}Male_{i}+\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta
      _{4}Tenure_{i}+\epsilon_{i}
    \end{equation*}
  \item Changing the reference group has no effect on the conclusions
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{The dummy variable trap}

  \begin{itemize}
  \item Consider the equation:%
    \begin{align*}
      \log \left( Wage_{i}\right) = & \beta _{0}+\delta _{0}Female_{i}+\gamma
      _{0}Male_{i}+ \\
      & +\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta _{4}Tenure_{i}+
      \epsilon_{i} 
    \end{align*}
  \item The intercept is like a regressor that takes the value one for
    all observations
  \item Since $Female_{i}+Male_{i}-1=0$ for 
    \color{red}%
    all 
    \color{black}%
    observations $i,$ we have 
    \color{red}%
    perfect multicollinearity%
    \color{black}%
    , and such an equation cannot be estimated.
  \item Cannot include an intercept and dummies for all groups!
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{The dummy variable trap}
  \begin{itemize}
  \item One of the dummies has to be omitted and the corresponding group
    becomes the \textbf{reference} group:
    \begin{itemize}
    \item Men are the reference group:
      \[\log \left( Wage_{i}\right) =\beta _{0}+\delta
      _{0}Female_{i}+\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta
      _{4}Tenure_{i}+\epsilon_{i}\]%
    \item Women are the reference group:
      \[\log \left( Wage_{i}\right) =\theta
      _{0}+\gamma _{0}Male_{i}+\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta
      _{4}Tenure_{i}+\epsilon_{i}\]%
    \end{itemize}
  \item Alternatively, one can include both dummies 
    \color{red}%
    without 
    \color{black}%
    the intercept:
    \[\log \left( Wage_{i}\right) =\pi _{0}Female_{i}+\pi
      _{1}Male_{i}+\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta
      _{4}Tenure_{i}+\epsilon_{i}\]%
    but this is less common
  % \item In R regression with no intercept can be estimated by including
  %   -1 in the formula
  %   \begin{lstlisting}
  %     lm(Wage ~ -1 + Female + Male + Educ + Exper + Tenure)
  %   \end{lstlisting}
  \end{itemize}
\end{frame}


\begin{frame}[squeeze,shrink]\frametitle{A slope shift and interactions}
  \begin{itemize}
  \item Can allow the returns to education to be different for men and
    women:
    \begin{align*}
      \log \left( Wage_{i}\right) =
      &\beta _{0}+\delta _{0}Female_{i}+\beta
        _{1}Educ_{i}+\delta _{1}\left( Female_{i}\cdot Educ_{i}\right) + \\
      & +\beta _{3}Exper_{i}+\beta _{4}Tenure_{i}+\epsilon_{i}.
    \end{align*}
  \item The variable $\left( Female_{i}\cdot Educ_{i}\right) $ is called an 
    \alert{interaction}
  \item The equation for men ($Female_{i}=0$):
    \begin{equation*}
      \log \left( Wage_{i}^{
          \color{blue}
          M
          \color{black}
        }\right) =\beta _{0}+\beta _{1}Educ_{i}+\beta _{3}Exper_{i}+\beta
      _{4}Tenure_{i}+\epsilon_{i}.
    \end{equation*}
  \item The equation for women ($Female_{i}=1$):%
    \begin{align*}
      \log \left( Wage_{i}^{
      \color{blue} 
          F
      \color{black}
      }\right) = & \left( \beta _{0}+\delta _{0}\right) +\left( \beta _{1}+\delta
                   _{1}\right) Educ_{i}+ \\
                 & +\beta _{3}Exper_{i}+\beta _{4}Tenure_{i}+\epsilon_{i}.
    \end{align*}
  \item $\delta _{1}$ is the difference in return to
    education between women and men (the reference group) after controlling for
    experience and tenure
  \end{itemize}
\end{frame}

\begin{frame}[squeeze,shrink]\frametitle{A slope shift}

  \includegraphics[width=\textwidth]{w-fig72}
\end{frame}

\begin{frame}\frametitle{Example}
  \begin{itemize}
  \item Estimated equation:
    \begin{eqnarray*}
      \widehat{\log \left( Wage_{i}\right) } &=&
                                                 \begin{array}[t]{l}
                                                   0.389 \\ 
                                                   \left( 0.119\right)%
                                                 \end{array}%
      -%
      \begin{array}[t]{l}
        0.227 \\ 
        \left( 0.168\right)%
      \end{array}%
      Female_{i}+ \\
                                             &&+%
                                                \begin{array}[t]{l}
                                                  0.082 \\ 
                                                  \left( 0.008\right)%
                                                \end{array}%
      Educ_{i}-%
      \begin{array}[t]{l}
        0.0056 \\ 
        \left( 0.0131\right)%
      \end{array}%
      Female_{i}\cdot Educ_{i} \\
                                             &&+%
                                                \begin{array}[t]{l}
                                                  0.029 \\ 
                                                  \left( 0.005\right)%
                                                \end{array}%
      Exper_{i}-%
      \begin{array}[t]{l}
        0.00058 \\ 
        (0.00011)%
      \end{array}%
      Exper_{i}^{2}+ \\
                                             &&+%
                                                \begin{array}[t]{l}
                                                  0.032 \\ 
                                                  \left( 0.007\right)%
                                                \end{array}%
      Tenure_{i}-%
      \begin{array}[t]{l}
        0.00059 \\ 
        (0.00024)%
      \end{array}%
      Tenure_{i}^{2}.
    \end{eqnarray*}%
  \item $\hat{\delta}_{1}=-0.0056$ suggests that the return to education for
    women is 0.56\% less than for men, however it is not statistically
    significant
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Multiple categories}
  \begin{itemize}
  \item In the previous examples, $Educ$ was a quantitative variable: years of
    education
  \item Suppose now that instead the education variable is 
    \alert{ordinal}
    :%
    \begin{equation*}
      Education=\left\{ 
        \begin{array}{ll}
          1 & \text{if high-school dropout,} \\ 
          2 & \text{if high-school graduate,} \\ 
          3 & \text{if some college,} \\ 
          4 & \text{if college graduate,} \\ 
          5 & \text{if advanced degree.}
        \end{array}
      \right.
    \end{equation*}
  \item Only the order is important, and there is no meaning to the 
    particular values
  \item Adding such a variable to the regression will give a difficult
    to interpret result
  \end{itemize}
\end{frame}

\begin{frame}[shrink] \frametitle{Multiple categories}
  \begin{equation*}
    Education_{i}=\left\{ 
      \begin{array}{ll}
        1 & \text{if high-school dropout,} \\ 
        2 & \text{if high-school graduate,} \\ 
        3 & \text{if some college,} \\ 
        4 & \text{if college graduate,} \\ 
        5 & \text{if advanced degree.}%
      \end{array}%
    \right.
  \end{equation*}
  \begin{itemize}
  \item Define 5 new dummy variables:
    \begin{eqnarray*}
      E_{1,i} &=&\left\{ 
                  \begin{array}{cc}
                    1 & \text{if high-school dropout,} \\ 
                    0 & \text{otherwise.}%
                  \end{array}%
                        \right. E_{2,i}=\left\{ 
                        \begin{array}{cc}
                          1 & \text{if high-school graduate,} \\ 
                          0 & \text{otherwise.}%
                        \end{array}%
                              \right. \\
      E_{3,i} &=&\left\{ 
                  \begin{array}{cc}
                    1 & \text{if some college,} \\ 
                    0 & \text{otherwise.}%
                  \end{array}%
                        \right. E_{4,i}^{{}}=\left\{ 
                        \begin{array}{cc}
                          1 & \text{if college graduate,} \\ 
                          0 & \text{otherwise.}%
                        \end{array}%
                              \right. \\
      E_{5,i}^{{}} &=&\left\{ 
                       \begin{array}{cc}
                         1 & \text{if advanced degree,} \\ 
                         0 & \text{otherwise.}%
                       \end{array}%
                             \right.
    \end{eqnarray*}%
  \item To avoid multicollinearity, one of the dummies has to be omitted:
    \begin{equation*}
      Wage_{i}=\beta _{0}+\delta _{0}Female_{i}+\delta _{2}E_{2,i}+\delta
      _{3}E_{3,i}+\delta _{4}E_{4,i}+\delta _{5}E_{5,i}+\text{Other Variables}
    \end{equation*}
  \item Group $1$ (high-school dropout) becomes the reference group.%
  \item $\delta _{2}$ measures the wage difference between high-school
    graduates and high-school dropouts
  \item $\delta _{3}$ measures the wage difference between individuals with
    some college education and high-school dropouts
  \item $\delta_3 - \delta_2$ is the wage difference between some
    college and high-school graduates
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% control variables vs regressor of interest

\begin{frame}[allowframebreaks]
  \frametitle{References}
\bibliographystyle{jpe}
\bibliography{../326}
\end{frame}

\end{document}




