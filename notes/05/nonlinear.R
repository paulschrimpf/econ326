rm(list=ls())
graphics.off()
require(ggplot2)
N <- 1000

cef <- function(x) {  exp(x)/(1+exp(x)) } ## true CEF
simulate <- function(n) {
  x <- rnorm(n)*5
  e <- rnorm(n)*.1
  y <- cef(x) + e
  df <- data.frame(x,y)
  df$cef <- cef(x)
  return(df)
}
df <- simulate(N)
p <- ggplot(data = df, aes(x = x, y = y)) +
  geom_smooth(method = "lm", se=FALSE, color="red", formula = y ~ x) +
  geom_point() + 
  geom_line(data=df, aes(x=x, y = cef), color="orange")
p
pdf("nonlinear.pdf")
p
dev.off()

## show finite sample bias
N <- c(seq(5,100,by=5),seq(200,1000,by=100))
S <- 500
bhat <- matrix(0,nrow=length(N),ncol=S)
for(i in 1:length(N)) {
  bhat[i,] <- replicate(S,lm(y ~ x, data=simulate(N[i]))$coef[2])
}
bf <- data.frame(N,rowMeans(bhat))
names(bf) <- c("N","mean.beta")
p <- ggplot(data=bf,aes(x=N,y=mean.beta)) + geom_line(color="red") +
pdf("finiteBias.pdf")
p
dev.off()




