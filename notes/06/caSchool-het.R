rm(list=ls())
graphics.off()
require(foreign)
require(ggplot2)
require(sandwich)
require(lmtest)
## load data
data.file.name <- "caschool.dta" 
if (!file.exists(data.file.name)) {
  download.file(url="http://wps.aw.com/wps/media/objects/3254/3332253/datasets2e/datasets/caschool.dta",
                destfile=data.file.name, mode="wb")
}
ca <- read.dta(data.file.name)

## test scores on student-teacher ratio
summary(lm(testscr ~ str, data=ca))
summary(lm(testscr ~ str + I(str^2), data=ca))

require(texreg)
models <- list(lm(testscr ~ str, data=ca),
               lm(testscr ~ str + avginc, data=ca),
               lm(testscr ~ str + avginc + el_pct, data=ca))
texTable <-  texreg(models,
                    model.names=c("1","2","3"),
                    custom.names=c("$\\beta_0$","$\\frac{student}{teacher}$",
                      "Income",
                      "English learners"),                      
                    use.packages=FALSE,
                    caption="Test scores and student teacher ratios",
                    label="tab:caTest")
cat(texTable,file="caTest.tex.in")

lapply(models, FUN=function(m)  { sqrt(diag(vcovHC(m))) } )

## plots of residuals
ca$residuals <- residuals(models[[3]])
ca$yHat <- predict(models[[3]])
fdims <-1.5*c(5.03937, 3.77953) 
textSize <- 24
pdf("cas-str.pdf",width=fdims[1],height=fdims[2])
ggplot(data=ca, aes(x=str,y=residuals)) + geom_point() +
  theme(axis.title.x = element_text(size = textSize),
        axis.title.y = element_text(size = textSize))
dev.off()

pdf("cas-inc.pdf",width=fdims[1],height=fdims[2])
ggplot(data=ca, aes(x=avginc,y=residuals)) + geom_point() +
  theme(axis.title.x = element_text(size = textSize),
        axis.title.y = element_text(size = textSize))
dev.off()


pdf("cas-el.pdf",width=fdims[1],height=fdims[2])
ggplot(data=ca, aes(x=el_pct,y=residuals)) + geom_point() +
  theme(axis.title.x = element_text(size = textSize),
        axis.title.y = element_text(size = textSize))
dev.off()

pdf("cas-yhat.pdf",width=fdims[1],height=fdims[2])
ggplot(data=ca, aes(x=yHat,y=residuals)) + geom_point() +
  theme(axis.title.x = element_text(size = textSize),
        axis.title.y = element_text(size = textSize))
dev.off()







