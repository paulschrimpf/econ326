## R file recreating parts of Angrist and Krueger (1991) and Card (1995)
library(sandwich) ## for vcovHC
library(lmtest)   ## for coeftest
library(AER)      ## for ivreg


## load Angrist Krueger data (originally from 1970 and 1980 census of US)
## data downloaded from http://economics.mit.edu/files/2854
if (!exists("ak")) {
  library(foreign)
  ak <- read.dta("NEW7080.dta")
}
names(ak) <- c("age","ageq","v3","educ","ENOCENT","ESOCENT",
               "v7","v8","lwkwage","married","midatl","mt","neweng","v14","v15",
               "census","v17",
               "QOB","race","smsa","soatl","v22","v23",
               "wnocent","wsocent","v26","yob")
ak$yob[ak$yob<1900] <- ak$yob[ak$yob<1900]+1900
ak$yqob <- ak$yob + (ak$QOB-1)/4
ak$cohort <- 20.29
ak$cohort[ak$yob<=1939 & ak$yob>=1930] <- 30.39
ak$cohort[ak$yob<=1949 & ak$yob>=1940] <- 40.49

## load Card data (originally from NLS Young Men 1966 cohort, provided
## by Wooldridge textbook)
load("card.RData")
card <- data
rm(data)

## ols regression

akOLS <- lm(lwkwage ~ educ, data=ak)
cardOLS <- lm(log(wage) ~ educ, data=card)

coeftest(akOLS, vcov=vcovHC(akOLS,type="HC0"))
coeftest(cardOLS, vcov=vcovHC(cardOLS,type="HC0"))

## make texreg report heteroskedasticity robust standard errors
library(texreg)
extract.lm.robust <- function(model,...) {
  tr <- extract.lm(model)
  robust <- coeftest(model,vcovHC(model,type="HC0"))
  tr@se <- robust[,2]
  tr@pvalues <- robust[,4]
  return(tr)
}
extract.list <- function(model, ...) {
  return(lapply(model,extract))
}
setMethod("extract", signature=className("list"),
          definition = extract.list)
setMethod("extract", signature=className("lm", "stats"),
          definition = extract.lm.robust)

## create table of regression results
models <- list(akOLS,cardOLS)
texTable <-  texreg(models,
                    custom.model.names=c("AK","Card"),
                    custom.names=c("(Intercept)","Education"),
                    use.packages=FALSE,
                    caption="OLS estimates",
                    digits=4,label="ols",return.string=TRUE)
cat(texTable,file="educ-ols.tex")


## Card (1995) IV estimate
## first stage
cat(texreg(lm(educ ~ nearc4,data=card),
           single.row=TRUE,table=FALSE,use.packages=FALSE,return.string=TRUE),
    file="educ-cardFirstStage.tex")
## IV
cardIV <- ivreg(log(wage) ~ educ | nearc4, data=card)
## for texreg
extract.ivreg <- function(model, include.nobs=TRUE, ...) {
  s <- summary(model, ...)
  ct <- coeftest(model,vcovHC(model,type="HC0"))
  names <- rownames(s$coef)
  co <- s$coef[, 1]
  se <- ct[, 2]
  pval <- ct[, 4]
  n <- nobs(model)
  gof <- numeric()
  gof.names <- character()
  gof.decimal <- logical()
  if (include.nobs == TRUE) {
    gof <- c(gof, n)
    gof.names <- c(gof.names, "Num. obs.")
    gof.decimal <- c(gof.decimal, FALSE)
  }
  tr <- createTexreg(coef.names = names, coef = co, se = se,
                     pvalues = pval, gof.names = gof.names, gof = gof, gof.decimal = gof.decimal)
  return(tr)
}
setMethod("extract", signature=className("ivreg", "AER"),
          definition = extract.ivreg)
cat(texreg(cardIV,single.row=TRUE,custom.names=c("(Intercept)","Education"),
           table=TRUE,use.packages=FALSE,
           caption="Card IV estimates",digits=4,label="cardIV",return.string=TRUE),
    file="educ-cardIV.tex")


## Angrist Krueger IV estimate
cat(texreg(lm(educ ~ QOB,data=ak),
           single.row=TRUE,table=FALSE,use.packages=FALSE,return.string=TRUE),
    file="educ-akFirstStage.tex")
akIV <- list(ivreg(lwkwage ~ educ | QOB, data=ak),
             ivreg(lwkwage ~ educ | QOB,
                   data=subset(ak,cohort==20.29)),
             ivreg(lwkwage ~ educ | QOB,
                   data=subset(ak,cohort==30.39)),
             ivreg(lwkwage ~ educ | QOB,
                   data=subset(ak,cohort==40.49)))
cat(texreg(akIV,single.row=FALSE,
           custom.model.names=c("All","1920-29","1930-39","1940-49"),
           custom.names=c("(Intercept)","Education"),
           table=TRUE,use.packages=FALSE,
           caption="Angrist \\& Krueger IV estimates",digits=4,label="akIV",return.string=TRUE),
    file="educ-akIV.tex")
akOLS <- list(lm(lwkwage ~ educ , data=ak),
              lm(lwkwage ~ educ ,
                    data=subset(ak,cohort==20.29)),
              lm(lwkwage ~ educ ,
                    data=subset(ak,cohort==30.39)),
              lm(lwkwage ~ educ ,
                    data=subset(ak,cohort==40.49)))
cat(texreg(akOLS,single.row=FALSE,
           custom.model.names=c("All","1920-29","1930-39","1940-49"),
           custom.names=c("(Intercept)","Education"),
           table=TRUE,use.packages=FALSE,
           caption="Angrist \\& Krueger OLS estimates",digits=4,label="akIV",return.string=TRUE),
    file="educ-akOLS.tex")

## graph E[educ|yqob]
library(data.table)
ymean <-
  data.table(ak)[,list(educ=mean(educ), lwage=mean(lwkwage),
                       qob=mean(QOB),se=sd(educ)/sqrt(sum(!is.na(educ))),
                       sw=sd(lwkwage)/sqrt(sum(!is.na(lwkwage))) )
                 , by=yqob]
library(ggplot2)
textSize <- 24
fsPlot <- ggplot(data=NULL,aes(x=ymean$yqob,
                   y=ymean$educ,label=ymean$qob)) +
  geom_text() + geom_line(alpha=0.7,colour="red") +
  geom_line(alpha=0.5,colour="orange",aes(y=ymean$educ+1.96*ymean$se)) +
  geom_line(alpha=0.5,colour="orange",aes(y=ymean$educ-1.96*ymean$se)) +
  scale_x_continuous(name="Date of birth") +
  scale_y_continuous(name="Average years of education") +
  theme(axis.title.x = element_text(size = textSize),
        axis.title.y = element_text(size = textSize))

fsPlot
fdims <-1.5*c(5.03937, 3.77953)
pdf("ak-firstStage.pdf",width=fdims[1],height=fdims[2])
fsPlot
dev.off()

fsPlot <- ggplot(data=NULL,aes(x=ymean$yqob,
                   y=ymean$lwage,label=ymean$qob)) +
  geom_text() + geom_line(alpha=0.7,colour="red") +
  geom_line(alpha=0.5,colour="orange",aes(y=ymean$lwage+1.96*ymean$sw)) +
  geom_line(alpha=0.5,colour="orange",aes(y=ymean$lwage-1.96*ymean$sw)) +
  scale_x_continuous(name="Date of birth") +
  scale_y_continuous(name="Average log wage") +
  theme(axis.title.x = element_text(size = textSize),
        axis.title.y = element_text(size = textSize))

fsPlot
pdf("ak-reducedForm.pdf",width=fdims[1],height=fdims[2])
fsPlot
dev.off()

######################################################################
## Part II: understanding why estimates differ

## controlling for age
olsAge <- list(
  lm(lwkwage ~ educ + age + I(age^2) , data=subset(ak,cohort==20.29)),
  lm(lwkwage ~ educ + age + I(age^2) , data=subset(ak,cohort==30.39)),
  lm(log(wage) ~ educ + age + I(age^2) , data=card)
  )
texTable <-  texreg(olsAge,
                    custom.model.names=c("AK 20-29","AK 30-39","Card"),
                    custom.names=c("(Intercept)","Education","Age","Age$^2$"),
                    use.packages=FALSE,
                    caption="OLS estimates",
                    digits=4,label="olsAge",return.string=TRUE)
cat(texTable,file="educAge-ols.tex")
ivAge <- list(ivreg(lwkwage ~ educ + age + I(age^2) |
                    QOB + age + I(age^2),
                    data=subset(ak,cohort==20.29)),
              ivreg(lwkwage ~ educ + age + I(age^2) |
                    QOB + age + I(age^2),
                    data=subset(ak,cohort==30.39)),
              ivreg(log(wage) ~ educ + age + I(age^2) |
                    nearc4 + age + I(age^2), data=card)
              )
texTable <-  texreg(ivAge,
                    custom.model.names=c("AK 20-29","AK 30-39","Card"),
                    custom.names=c("(Intercept)","Education","Age","Age$^2$"),
                    use.packages=FALSE,
                    caption="IV estimates",
                    digits=4,label="ivAge",return.string=TRUE)
cat(texTable,file="educAge-iv.tex")

## create south region in AK data
ak$south <- ak$ESOCENT + ak$soatl + ak$wsocent
## IV controlling for south and smsa (urban)
ivAU <- list(ivreg(lwkwage ~ educ + age + I(age^2) + smsa + south|
                    QOB + age + I(age^2) + smsa + south,
                    data=subset(ak,cohort==20.29)),
             ivreg(lwkwage ~ educ + age + I(age^2) + smsa + south|
                   QOB + age + I(age^2) + smsa + south,
                   data=subset(ak,cohort==30.39)),
             ivreg(log(wage) ~ educ + age + I(age^2) + smsa + south |
                   nearc4 + age + I(age^2) + smsa + south, data=card),
             ivreg(log(wage) ~ educ + age + I(age^2) + smsa66 + south66 |
                   nearc4 + age + I(age^2) + smsa66 + south66, data=card)
             )
texTable <-  texreg(ivAU,
                    custom.model.names=c("AK 20-29","AK 30-39","Card","Card (geo 1966)"),
                    custom.names=c("(Intercept)","Education","Age","Age$^2$",
                      "Urban","South","Urban 1966","South 1966"),
                    use.packages=FALSE,
                    caption="IV estimates",
                    digits=4,label="ivAgeUrban",return.string=TRUE)
cat(texTable,file="educAgeUrban-iv.tex")

## AK estimates with dummy instruments
ivDum <- list(
  ivreg(lwkwage ~ educ + age + I(age^2) + smsa + south| factor(QOB) + age +
        I(age^2) + smsa + south, data=subset(ak,cohort==30.39)),
  ivreg(lwkwage ~ educ + age + I(age^2) + smsa + south|
factor(QOB)*factor(age) + smsa + south, data=subset(ak,cohort==30.39))
  )
texTable <-  texreg(ivDum,
                    custom.model.names=c("QOB", "QOB $\\times$ YOB"),
                    custom.names=c("(Intercept)","Education","Age","Age$^2$",
                      "Urban","South"),
                    use.packages=FALSE,
                    caption="AK 1930-1939 IV estimates",
                    digits=4,label="ivAgeUrban",return.string=TRUE)
cat(texTable,file="educDum-iv.tex")



