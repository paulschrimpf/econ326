## These functions also produce heteroskedasticity robust clustered
## standard errors. They are adapted from
## http://people.su.se/~ma/mcluster.R
## The results of cl.plm are identical vcovClusterHC above
cl <- function(dat,fm,cluster) {
  ef <- estfun(fm)
  if (length(cluster)!=nrow(ef)) {
    cluster <- cluster[as.numeric(rownames(ef))]
  }

  M <- length(unique(cluster))
  N <- length(cluster)
  dfc <- (M/(M-1))*((N-1)/(N-fm$rank))
  u <- apply(estfun(fm),2,
             function(x) tapply(x, cluster, sum))
  vcovCL <- dfc*sandwich(fm, meat=crossprod(u)/N)
  coeftest(fm, vcovCL)
}

cl.plm   <- function(dat,fm, cluster)
{
  require(sandwich, quietly = TRUE)
  require(lmtest, quietly = TRUE)
  ef <- estfun.plm(fm)
  if (length(cluster)!=nrow(ef)) {
    cluster <- cluster[as.numeric(rownames(ef))]
  }
  M <- length(unique(cluster))
  N <- length(cluster)
  K <- ncol(ef)
  dfc <- (M/(M-1))*((N-1)/(N-K))
  
  uj  <- apply(ef,2, function(x) tapply(x, cluster, sum))
  bread <- bread.plm(fm)
  meat <- crossprod(uj)
  vcovCL <- dfc*bread %*% meat %*% bread
  coeftest(fm, vcovCL)
}

estfun.plm <- function(x, ...) {
  model <- x$args$model
  formula <- formula(x)
  if (length(formula)[2]>1) { ## 2SLS
    data <- model.frame(x)
    effect <- x$args$effect
    X <- model.matrix(formula, data, rhs = 1, model = model, 
                      effect = effect, theta = theta)
    if (length(formula)[2] == 2) 
      W <- model.matrix(formula, data, rhs = 2, model = model, 
                        effect = effect, theta = theta)
    else 
      W <- model.matrix(formula, data, rhs = c(2,3), model = model, 
                        effect = effect, theta = theta)
    Xhat <- lm(X ~ W)$fit
    return(x$residuals*Xhat)
  } else { ## OLS 
    X <- model.matrix(x, model = model)
    return( x$residuals * X)
  }
}


bread.plm <- function(x, ...) {
  model <- x$args$model
  formula <- formula(x)
  if (length(formula)[2]>1) { ## 2SLS
    data <- model.frame(x)
    effect <- x$args$effect
    X <- model.matrix(formula, data, rhs = 1, model = model, 
                      effect = effect, theta = theta)
    if (length(formula)[2] == 2) 
      W <- model.matrix(formula, data, rhs = 2, model = model, 
                        effect = effect, theta = theta)
    else 
      W <- model.matrix(formula, data, rhs = c(2,3), model = model, 
                        effect = effect, theta = theta)
    Xhat <- lm(X ~ W)$fit
    return( solve(crossprod(Xhat)) )    
  } else { ## OLS
    X <- model.matrix(x, model = model)
    return( solve(crossprod(X)) )
  }
}
