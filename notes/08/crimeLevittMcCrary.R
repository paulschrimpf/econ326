rm(list=ls())
require(foreign)
require(AER)
require(ggplot2)
require(data.table)


mldata <- read.dta("mccrary-crime/data/crime2.dta")
mldata <- subset(mldata,year>=1970 & year<=1992) ## get rid of years
                                                 ## without crime data

crimes <-
  c("murder","rape","robbery","assault","burglary","larceny","auto")
for(c in crimes) {
  mldata[,paste(c,"rate",sep=".")] <- mldata[,c]/mldata$citypop*1e5
}
mldata$police.per100k <- mldata$sworn/mldata$citypop*1e5

## cost of crime for each type from RAND
## http://www.rand.org/jie/centers/quality-policing/cost-of-crime.html
crimeWeights <- c(8649216, 217866, 67277, 87238, 13096, 2139, 9079)
names(crimeWeights) <- crimes
foo <- as.matrix(mldata[,crimes]) %*% as.vector(crimeWeights)
mldata$crime <- as.numeric(foo)
mldata$violent.crime <- as.numeric(as.matrix(mldata[,crimes]) %*%
  as.vector(crimeWeights*c(1,1,1,1,0,0,0)))
mldata$property.crime <- as.numeric(as.matrix(mldata[,crimes]) %*%
  as.vector(crimeWeights*(1-c(1,1,1,1,0,0,0))))
## crime is a cost weighted sum of different crimes
mldata$crime.rate <- mldata$crime/mldata$citypop
mldata$violent.crime.rate <- mldata$violent.crime/mldata$citypop
mldata$property.crime.rate <- mldata$property.crime/mldata$citypop

mldata$welf.pc <- mldata$sta_welf ## welfare per capita
mldata$educ.pc <- mldata$sta_educ ## education per capita
mldata$a15_24 <- mldata$a15_19+mldata$a20_24
mldata$city.size <- as.factor(1*(mldata$citypop>=2.5e5) +
                              1*(mldata$citypop>=5e5)+1*(mldata$citypop>=1e6))


## plot average crime rate by year
if (TRUE) {
  library(ggplot2)
  fdims <-1.5*c(5.03937, 3.77953)
  yearAvg <- aggregate(mldata, by = list(mldata$year), FUN=mean,
                       na.rm=TRUE)
  yearAvg <- yearAvg[order(yearAvg$year),] ## sort by year

  pdf("fig/crimeRateYear.pdf",width=fdims[1],height=fdims[2])
  ggplot(data=yearAvg,aes(x=year,y=crime.rate)) + geom_line() + theme_minimal()
  dev.off()

  for(c in crimes) {
    p <- ggplot(data=yearAvg,
                aes_string(x="year",y=paste(c,".rate",sep=""))) +
                  geom_line() + theme_minimal()
    pdf(paste("fig/",c,"RateYear.pdf",sep=""),width=fdims[1],height=fdims[2])
    print(p)
    dev.off()
  }

  pdf("fig/policeRateYear.pdf",width=fdims[1],height=fdims[2])
  ggplot(data=yearAvg,aes(x=year,y=police.per100k)) + geom_line() + theme_minimal()
  dev.off()

  #pdf("fig/dpoliceRateYear.pdf",width=fdims[1],height=fdims[2]/2)
  #ggplot(data=yearAvg,aes(x=year,y=dpolice.per100k)) + geom_line() + theme_minimal()
  #dev.off()
}

## OLS regressions
require(plm) ## package for panel data
require(lmtest) ## for coeftest
require(sandwich)
source("clusterFunctions.R")

mldata$region <- floor(mldata$censdist/10)
mldata$region[mldata$jid==33] <- 5 ## Washington DC
mldata$region <- factor(mldata$region)
lags <- 0:1
ols.fmla<- formula(log(crime.rate) ~
                    lag(log(police.per100k),lags)
                    + year + unemp + log(welf.pc) +
                    log(educ.pc) + citybla + cityfemh + a15_24
                    )

iv.fmla <- formula(log(crime.rate) ~
                   lag(log(police.per100k),lags)
                   + year + unemp + log(welf.pc) +
                   log(educ.pc) + citybla + cityfemh + a15_24
                   | .
                   - lag(log(police.per100k),lags)
                   + (lag(elecyear,lags) + lag(governor,lags)) )
ols.wAll <- plm(ols.fmla,data=mldata,model="pooling",index=c("city","year"))
coeftest(ols.wAll)
cl.plm(mldata, ols.wAll, mldata$city) ## clustered s.e.
fd.wAll <- plm(ols.fmla,data=mldata,model="fd",index=c("city","year"))
coeftest(fd.wAll)
cl.plm(mldata, fd.wAll, mldata$city) ## clustered s.e.
tsls.wAll <- plm(iv.fmla, data=mldata, model="fd", index=c("city","year"))
coeftest(tsls.wAll) ##
cl.plm(mldata, tsls.wAll, mldata$city) ## clustered s.e.

## Here, we will use the exact same specification as in Table 3 of
## Levitt (1997)
## SUR type estimates
drop <- sapply(c("violent","property",crimes),FUN = function (x) { grep(x,names(mldata)) } )
pool <- mldata[,-drop]
pool$crime <- mldata$murder
pool$crime.name <- "murder"
for(c in 2:length(crimes)) {
  tmp <- mldata[,-drop]
  tmp$crime <- mldata[,crimes[c]]
  tmp$crime.name <- crimes[c]
  pool <- rbind(pool,tmp)
}
pool$crime.rate <- pool$crime/pool$citypop*1e5
pool$id <- pool$city + 100*as.numeric(factor(pool$crime.name))
pool$violent <- pool$crime.name=="murder" |
  pool$crime.name=="rape" | pool$crime.name=="assault" |
  pool$crime.name=="robbery"

fmla <- formula(log(crime.rate) ~
                violent*lag(log(police.per100k),lags) +
                unemp + log(welf.pc) +
                log(educ.pc) + citybla + cityfemh + a15_24 +
                factor(crime.name):region + year + factor(city))
ols <- plm(fmla, data=pool, model="pooling", index=c("id","year"))
fd <- plm(fmla, data=pool, model="fd", index=c("id","year"))
cl.plm(pool,fd,pool$city)

iv.fmla <- formula(log(crime.rate) ~
                violent*lag(log(police.per100k),lags) +
                unemp + log(welf.pc) +
                log(educ.pc) + citybla + cityfemh + a15_24 +
                year + factor(crime.name) + factor(city) | .
                - violent*lag(log(police.per100k),lags)
                + violent*(lag(elecyear,lags) + lag(governor,lags)) )
tsls <- plm(iv.fmla, data=pool, model="fd", index=c("id","year"))
cl.plm(pool,tsls,pool$city)
coeftest(tsls)

sum.coef <- sum(tsls$coef[2:(1+length(lags))])
se <- sqrt(crossprod(rep(1,length(lags)),crossprod(tsls$vcov[2:(1+length(lags)),2:(1+length(lags))],rep(1,length(lags)))))


sum.coef <- sum(tsls.wAll$coef[2:(1+length(lags))])
se <- sqrt(crossprod(rep(1,length(lags)),crossprod(tsls.wAll$vcov[2:(1+length(lags)),2:(1+length(lags))],rep(1,length(lags)))))

