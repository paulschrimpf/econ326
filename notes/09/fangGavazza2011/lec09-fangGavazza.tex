\input{../../slideHeader}

\title{Example 2: dynamic inefficiencies in employment-based health
  insurance}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 326} 
\date{\today}


\providecommand{\bols}{{\hat{\beta}^{\mathrm{OLS}}}}
\providecommand{\biv}{{\hat{\beta}^{\mathrm{IV}}}}
\providecommand{\btsls}{{\hat{\beta}^{\mathrm{2SLS}}}}

\graphicspath{{figures/}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}\frametitle{Plan}
  \begin{itemize}
  \item Last week and today: how to use the methods we've studied 
  \item Today: another walk-through research project
  \item This time, we will focus less on big picture issues (how to
    choose a topic, etc) and more on details and interpreting results
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item ``Dynamic Inefficiencies in an Employment-Based 
    Health Insurance System: Theory and Evidence'' \cite{fang2011}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Introduction}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item US health care:
    \begin{itemize}
    \item \alert{Private, employment-based insurance (55\%)} 
      \footnote{These numbers come from table 10 of \cite{denavas2011}. Some people
        have more than one form of insurance, so these percentages
        need not add to 100.} 
    \item Private direct purchase insurance (9.8\%)
    \item Retirees over 65: Medicare (14.5\%)
    \item Low income : Medicaid (15.9\%)
    \item Uninsured (16.3\%)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item Basic idea: 
    \begin{itemize}
    \item Health is a form of human capital 
    \item Firms and workers will want to invest in workers' health
    \item Possibility of worker turnover can lead to under investment
      in health
    \item Under investment in health while working $\Rightarrow$
      higher health spending in retirement
    \end{itemize}
  \end{itemize}
\end{frame}

%% Add details of model?

\begin{frame}
  \frametitle{Theoretic Predictions} 
  \begin{itemize}
  \item Workers in jobs with lower turnover rates have:
    \begin{enumerate}
    \item Higher medical expenditures while working
    \item Lower medical expenditures and better health during retirement
    \end{enumerate}
  \item \cite{fang2011} show that these are the implications of a
    simple model with labor market frictions (jobs end and part of the
    higher wage associated with better health is lost) and employer
    provided health investment
  \end{itemize}  
\end{frame}


\section{Data}
\begin{frame}[allowframebreaks]
  \frametitle{Data}
  \begin{itemize}
  \item Medical Expenditure Panel Survey (MEPS) household component 1995-2005
    \begin{itemize}
    \item Rotating panel (follows people for 2 years)
    \item Annual medical expenditure, number of doctor visits
    \item Job tenure, job industry, income
    \item Demographics: age, education, gender, race, marital
      status, household size, geographic region
    \end{itemize}
  \end{itemize} 
\end{frame}

\begin{frame}\frametitle{Data}
  \alert{Scenario}: we have gathered our data. What is the first thing
  we should do with it? 

  \vspace{12pt}

  We should check that it makes sense. Let's look at some tables and
  figures. What tables and figures should we create?
\end{frame}

\section{Empirical specification}

\begin{frame}\frametitle{Empirical specification}
  \begin{itemize}
  \item Want to test whether workers in jobs with lower turnover rates
    have: 
    \begin{enumerate}
    \item \alert{Higher medical expenditures while working}
    \item Lower medical expenditures and better health during
      retirement (we will look at this next)
    \end{enumerate}
  \item What equation do we want to estimate?
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{OLS}
  \begin{align*}
    \log (spend_{it}) = & \beta_0 + \beta_1 \log(Tenure_{it}) + \beta_x X_{it} + \\
    & + \text{region} \times \text{year dummies} + \zeta_i +
    \epsilon_{it} 
  \end{align*}
  \begin{itemize}
  \item What assumption do we need for OLS to be consistent?
  \item Suppose OLS is consistent, what is the predicted sign of
    $\bols_1$? 
  \item Why is OLS likely not consistent? In what direction do you
    think OLS will be biased?
  \end{itemize}
\end{frame}

%% Add question about measurement error and differencing?

\begin{frame}\frametitle{OLS - interpretation}
  How can we interpret the OLS estimates? Does the coefficient of
  interest have the expected sign? Is it small or large?
\end{frame}

\begin{frame}\frametitle{OLS - inference}
  How should we calculate standard errors? 
\end{frame}

\begin{frame}\frametitle{IV}
  We need an instrument for job tenure. What conditions does an
  instrument need to satisfy? What could be a good instrument?

  \pause
  \vspace{12pt}
    
  We will use the number of establishments that closed and jobs that 
  disappeared in a person's
  region, industry and year as instruments. What is the rationale for
  this instrument? Are there any reasons to think it is not a valid instrument?
\end{frame}

\begin{frame}
  \frametitle{IV}
  Establishment closures are a valid instrument if:
  \begin{enumerate}
  \item Relevant: ``The employer-employee pair forms expectations about plant
    closures that are correlated with their realizations, so that
    expected turnover and actual turnover generated by plant closures
    covary''
  \item Exogenous: ``Establishment deaths do not directly affect individual
    medical expenditures—i.e., the exclusion restriction''
  \end{enumerate}
\end{frame}

\begin{frame}\frametitle{2SLS - specification}
  How should we specify the 2SLS estimator? What is the
  dependent variable(s)? What is the endogenous regressor of interest?
  What is the instrument? What controls should we include? 
  
  \vspace{12pt}
 
  What is the first stage? What is the reduced form? What should we
  check in these regressions? 
\end{frame}

% \begin{frame}\frametitle{Step 4: 2SLS - interpretation and inference}
%   How can we interpret the estimates? Does the coefficient of
%   interest have the expected sign? Is it small or large?
  
%   \vspace{12pt}
    
%   How should we calculate standard errors? What hypothesis(es) should 
%   we test?  
% \end{frame}

% \begin{frame}\frametitle{Step 4: 2SLS - threats to validity}
%   Why might elections not be a valid instrument? Is there anything we
%   can check to reassure us that elections are exogenous?
% \end{frame}

% \begin{frame}\frametitle{Step 5: further results}
%   Assuming we have correctly estimated the effect of police on crime,
%   what could we do with our estimate? What is a relevant policy
%   question? What else do we need to know to answer that question? 
% \end{frame}


\begin{frame}\frametitle{Code and data}
  \begin{itemize}
  \item
    \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/09/fg.R?at=master}
    {Code for main results}
  \item \href{https://bitbucket.org/paulschrimpf/econ326/src/master/notes/08/clusterFunctions.R?at=master}
    {Code for calculating heteroskedasticity robust clustered standard
      errors}
  \item \href{http://www.aeaweb.org/articles.php?doi=10.1257/aer.101.7.3047}{Data}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{References}
\bibliographystyle{jpe}
\bibliography{../326}
\end{frame}
  

\end{document}


