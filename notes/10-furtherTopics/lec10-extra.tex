\input{../slideHeader}

\title{Additional topics}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 326} 
\date{\today}


\providecommand{\bols}{{\hat{\beta}^{\mathrm{OLS}}}}
\providecommand{\biv}{{\hat{\beta}^{\mathrm{IV}}}}
\providecommand{\btsls}{{\hat{\beta}^{\mathrm{2SLS}}}}

\graphicspath{{figures/}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

% \begin{frame}
%   \frametitle{References}
%   \begin{itemize}
%   \item \cite{w2013} chapter 15
%   \item \cite{ap2009} chapter 4
%   \item \cite{ap2014} chapters 3, 6
%   \end{itemize}
% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Panel Data}

\begin{frame}\frametitle{Panel Data}
  \begin{itemize}
  \item Same units observed repeatedly over time
  \item Examples
    \begin{itemize}
    \item Same individuals surveyed each year (NLSY, PSID, etc)
    \item Same firms surveyed each year 
    \item Cities in \cite{levitt1997}
    \item States in \cite{ams2016}
    \end{itemize}
  \item Seeing the same units repeatedly introduces dependence, but
    also allows us to deal with unobserved heterogeneity in a richer
    way 
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Panel Data}
  \begin{itemize}
  \item $N$ units, indexed by $i$, observed for $T$ periods, indexed
    by $t$
    \[ y_{it} = \beta_0 + \beta_1 x_{it} +
      \underbrace{\epsilon_{it}}_{\equiv \alpha_i + \gamma_t +
        u_{it}} \] 
  \item $\alpha_i$ = fixed (or individual) effects 
  \item $\gamma_t$ = time effects
  \item Consistency of OLS needs $\Er[\epsilon x] = 0$
  \item Fixed effects or first differencing needs $\Er[u x] = 0$
    \begin{itemize}
    \item Time invariant unobserved individual characteristics,
      captured by $\alpha_i$, can be correlated with $x$
    \item Time varying unobserved variables that affect everyone,
      captured by $\gamma_t$, can be correlated with $x$
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Weak instruments}
\begin{frame}[allowframebreaks]
  \frametitle{Weak instruments}
  \begin{itemize}
  \item References: \cite{imbens2007}, \cite{ap2014} pages 145-146
  \item In applications, instruments are sometimes barely relevant,
    i.e. $\widehat{\cov}(z,x) \neq 0$, but $\widehat{\cov}(z,x)$ not
    far from $0$ relative to its standard error
  \item Implications:
    \begin{itemize}
    \item Finite sample bias of $\hat{\beta}^{2SLS}$ is large
    \item Usual standard error leads to inaccurate inference (wrong
      standard error, incorrect p-values, incorrect confidence
      intervals)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Weak instruments --- formalization}
  \begin{itemize}
  \item Use different asymptotic approximation to better reflect
    barely relevant instruments:
    \begin{align*}
      y_i = & \beta_0 + \beta_1 x_i + \epsilon_i \\
      x_i = & \pi_0 + \pi_{1,n} z_i + u_i 
    \end{align*}
    where $\pi_{1,n} = \tilde{\pi}_1 / \sqrt{n}$
  \item Implications:
    \begin{itemize}
    \item $\btsls$ is biased in the same direction as $\bols$
    \item $\btsls$ not asymptotically normal with mean 0 and usual
      variance 
    \item Usual p-values and confidence regions for $\btsls$ invalid
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Weak instruments --- solutions}
  \begin{itemize}
  \item Possible solution(s):
    \begin{itemize}
    \item Use an estimator with better bias properties under weak
      identification --- e.g. LIML
    \item Conduct inference using an identification robust test ---
      Anderson-Rubin (AR), Conditional Likelihood Ratio (CLR), or
      Kleibergen's (each valid whether instrument is strong, weak, or
      completely irrelevant)
    \end{itemize}
  \item Practical advice:
    \begin{itemize}
    \item Always report first stage $F$ statistic for significance of
      coefficients on instruments --- rule of thumb: $F \geq 10$ is
      okay, $F < 10$ need to worry about weak instruments
      \begin{itemize}
      \item Justification: under weak instrument asymptotics, bias of
        2SLS and is < 10\% when $F \geq 10$.
      \end{itemize}
    \item If concerned about weak instruments:
      \begin{itemize}
      \item Report reduced form estimate (regression of $y$ on $z$)
      \item Use alternative estimator (LIML, k-Class)
      \item Use identification robust hypothesis tests and confidence
        intervals
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Heterogeneous treatment effects}

\begin{frame} \frametitle{Heterogeneous treatment effects}
  \begin{itemize}
  \item Typical linear model assumes $x$ has same effect on $y$ for
    everyone 
    \[ y_i = \beta_0 + \beta_1 x_i + \epsilon_i \]
  \item What if effect of $x_i$ on $y_i$ varies across people, e.g.
    \[ y_i = \beta_0 + \beta_{\alert{i}}  x_i + \epsilon_i \]
  \item To simplify assume a binary treatment
    \[ x_i = \begin{cases} 1 & \text{ if treated}
        \\ 0 & \text{ if not treated}
      \end{cases}
    \]
    e.g. whether graduated from university or not
    \pause
  \item If $x$ exogenous, then $\Er[\bols] = \Er[\beta_i]$
  \end{itemize}
\end{frame}

\begin{frame}[shrink] \frametitle{Heterogeneous treatment effects}
  \begin{itemize}
  \item If $x$ endogenous, assume we have a binary instrument $z_i$,
    e.g. randomly assign some people to zero tuition
  \item What will IV estimate?
    \pause
    \begin{itemize}
    \item Show
      \[ \hat{\beta}^{IV} =
        \frac{\widehat{\cov}(y,z)}{\widehat{\cov}(x,z)} =
        \frac{\hat{\Er}[y|z=1] - \hat{\Er}[y|z=0]}{\hat{\Er}[x|z=1] -
          \hat{\Er}[x|z=0]} \]
      \pause
    \item Potential outcomes for $x$, $x_i^0 = $ value of $x_i$ if
      $z_i$ had been $0$, $x_i^1 = $ value of $x_i$ if $z_i$ had been
      $1$
      \pause
    \item Show
      \[ \plim \hat{\beta}^{IV} = \frac{\Er[\beta_i | x_i^1 = 1, x_i^0
          = 0]\Pr(x_i^1=1,x_i^0 = 0) - \Er[\beta_i | x_i^1 = 0, x_i^1
          = 0]\Pr(x_i^1=0,x_i^0 = 1) }{ \Pr(x_i^1=1,x_i^0 = 0)  -
          \Pr(x_i^1=0, x_i^0 = 1) }  \]
      \pause
    \item Assume no ``non-compliers'' :  $ \Pr(x_i^1=0, x_i^0 = 1)= 0$
    \item Conclude
      \[ \plim \hat{\beta}^{IV} = \Er[\beta_i | x_i^1 = 1, x_i^0
        = 0] \]
      IV estimates average effect of $x$ among people for whom the
      instrument changed $x$
    \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Simultaneous Equations}

\begin{frame} \frametitle{Simultaneous Equations}
  \begin{itemize}
  \item Economic quantities often determined by equilibrium
    relationships
    \begin{itemize}
    \item Demand and supply
    \item Competitive equilibrium
    \item Nash equilibrium
    \end{itemize}
  \item Equilibrium outcome satisfies a system of equations
  \item References: \cite{w2013} chapter 15, \cite{angrist2000}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Example: demand \& supply}
  \begin{itemize}
  \item Supply: $q^s = \beta_0^s + \beta_1^s p^s + \epsilon^s$
  \item Demand: $q^d = \beta_0^d + \beta_1^d p^d + \epsilon^d$
  \item Equilibrium: $q^s = q^d = q$ and $p^s = p^d = p$
  \item Inverse demand: $p^d = \frac{-\beta_0^d}{\beta_1^d} +
    \frac{1}{\beta_1^d} q^d + \frac{-\epsilon^d}{\beta_1^d}$
  \item Observed $p$ and $q$ satisfy:
    \begin{align*}
      q = & \beta_0^s + \beta_1^s p + \epsilon^s \\
      p = & \frac{-\beta_0^d}{\beta_1^d} +\frac{1}{\beta_1^d} q +
            \frac{-\epsilon^d}{\beta_1^d}
    \end{align*}
  \end{itemize} 
\end{frame}

\begin{frame} \frametitle{Generic setup}
  \begin{itemize} 
  \item Two equation system:
    \begin{align*}
      y_{i1} = & \beta_{01} + \alpha_{21} y_{i2} + \beta_{11} z_{i1} +
                 \beta_{21} z_{i2} + \epsilon_{i1} \\
      y_{i2} = & \beta_{02} + \alpha_{12} y_{i1} + \beta_{12} z_{i1} +
                 \beta_{22} z_{i2} + \epsilon_{i2} 
    \end{align*}
  \item Assume $z_{i1}$ and $z_{i2}$ are exogenous --- $\Er[z_{i1}
    \epsilon_{i1}] = 0$, $\Er[z_{i1} \epsilon_{i2} = 0]$, $\Er[z_{i2}
    \epsilon_{i1}] = 0$, $\Er[z_{i2} \epsilon_{i2} = 0]$
  \item $y_{i1}$ and $y_{i2}$ are endogenous
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Identification}
  \begin{itemize}
  \item Identification: given the distribution of observable variables
    ($y$ and $z$), are there unique values of the parameters ($\beta$
    and $\alpha$) that satisfy the model?
    \begin{itemize}
    \item In OLS and 2SLS the rank condition ensures identification
    \end{itemize}
  \item Reduced form: solve for $y$
    {\scriptsize{
        \begin{align*}
          y_{i1} = & \underbrace{\frac{\beta_{01} + \alpha_{21} \beta_{02}}
                     {1 - \alpha_{21} \alpha_{12}}}_{\pi_{01}} + 
                     \underbrace{\frac{\beta_{11} + \alpha_{21} \beta_{12}}
                     {1 - \alpha_{21} \alpha_{12}}}_{\pi_{11}} z_{i1} + 
                     \underbrace{\frac{\beta_{21} + \alpha_{21} \beta_{22}}
                     {1 - \alpha_{21} \alpha_{12}}}_{\pi_{21}} z_{i2} +
                     \underbrace{\frac{\epsilon_{i1} + \alpha_{21}
                     \epsilon_{i2}} 
                     {1 - \alpha_{21} \alpha_{12}}}_{u_{i1}} \\
          y_{i2} = & \underbrace{\frac{\beta_{02} + \alpha_{12} \beta_{01}}
                     {1 - \alpha_{12} \alpha_{21}}}_{\pi_{02}} + 
                     \underbrace{\frac{\beta_{12} + \alpha_{12} \beta_{21}}
                     {1 - \alpha_{12} \alpha_{21}}}_{\pi_{12}} z_{i1} + 
                     \underbrace{\frac{\beta_{22} + \alpha_{12} \beta_{21}}
                     {1 - \alpha_{12} \alpha_{21}}}_{\pi_{22}} z_{i2} +
                     \underbrace{\frac{\epsilon_{i2} + \alpha_{12}
                     \epsilon_{i1}} 
                     {1 - \alpha_{12} \alpha_{21}}}_{u_{i2}} 
        \end{align*} 
      }}
  \item No identification without some restriction 
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Identification}
  \begin{itemize}
  \item Equation 1 identified if there is an exogenous variable
    excluded from equation 1 and included in equation 2
    \begin{itemize}
    \item E.g. $\beta_{21} = 0$ and $\beta_{22} \neq 0$. 
    \item Intuition: can use $z_{i2}$ as instrument for $y_{i2}$ and
      estimate equation 1 by 2SLS
    \end{itemize}
  \item Equation 2 identified if there is an exogenous variable
    excluded from equation 2 and included in equation 1
    \begin{itemize}
    \item E.g. $\beta_{12} = 0$ and $\beta_{11} \neq 0$. 
    \item Intuition: can use $z_{i1}$ as instrument for $y_{i1}$ and
      estimate equation 2 by 2SLS
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Limited Dependent Variables}

\begin{frame}\frametitle{Limited dependent variables}
  \begin{itemize}
  \item Limited dependent variables $=$ dependent variable has limited
    range
  \item Examples:
    \begin{itemize}
    \item Binary $y_i = 0$ or $1$, such as employment, program
      participation, etc
    \item Multinomial $y_{i}$ one of a finite number of categories,
      such as which type of car someone buys
    \item Censoring and truncation $y_{i} \geq 0$, such as hours
      worked
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Binary dependent variables}
  \begin{itemize}
  \item Observed: $y_{i} = 0$ or $1$
  \item Model: 
    \begin{align*}
      y_{i}^* = & \beta_0 + \beta_1 x_i + \epsilon_i \\
      y_{i} = & \begin{cases} 
        1 & \text{ if } y_i^* \geq 0 \\
        0 & \text{ if } y_i^* < 0
      \end{cases}
    \end{align*}
  \item Assume $\epsilon \indep x$ and $\epsilon$ has CDF $F_\epsilon$
    \begin{align*}
      \Pr(y=1|x) = & \Pr(\beta_0 + \beta_1 x + \epsilon \geq 0 | x) \\
      = & \Pr(\epsilon \geq -(\beta_0 + \beta_1 x) |x) \\
      = & 1-F_\epsilon(-\beta_0 - \beta_1 x)
    \end{align*}
  \item $\beta_0$ and $\beta_1$ estimated by maximum likelihood:
    {\scriptsize{
    \begin{align*}
      (\hat{\beta}_0, \hat{\beta}_1) = \argmax_{b_0,b_1} \sum_{i=1}^N
      \left[\log F_{\epsilon}(-b_0 - b_1 x_i) (1-y_i) +  \log \left(1-
      F_{\epsilon}(-b_0 - b_1 x_i)\right)y_i \right]
    \end{align*}
  }}
  \item When $\epsilon \sim N(0,1)$, called a probit
  \item When $F_\epsilon(z) = \frac{e^z}{1+e^z}$, called a logit
  \end{itemize}
\end{frame}

\subsection{Selection}

\begin{frame}[allowframebreaks]
  \frametitle{Selection}
  \begin{itemize}
  \item Continuous outcome $y_i$ observed only if a binary outcome,
    $d_i = 1$
    \begin{itemize}
    \item E.g. $y_i = \log $ wage and $d_i = 1$ if employed, $0$ if
      unemployed
    \end{itemize}
  \item Model: 
    \begin{align*}
      d_i = & \begin{cases} 1 & \text{ if } \gamma_0 + \gamma_1 x_i +
        \gamma_2 z_i +
        u_i \geq 0 \\
        0 & \text{ if } \gamma_0 + \gamma_1 x_i + \gamma_2 z_i + u_i < 0 
      \end{cases} \\
      y_i = & \begin{cases} \beta_0 + \beta_1 x_i + \epsilon_i & \text{ if
        } d_i = 1 \\
        \text{ not observed } & \text{ if } d_i = 0
      \end{cases}
    \end{align*}
    \begin{itemize}
    \item Assume $\epsilon, u \indep x,z$
    \item $\epsilon$ and $u$ might be correlated
    \end{itemize}
  \item OLS using observed $y$ is inconsistent
    \begin{align*}
      \plim \bols_1 = & \frac{\cov(x,y | d=1)}{\var(x|d=1)} \\
      = & \beta_1 + \frac{\cov(x,\epsilon | d=1)}{\var(x|d=1)} \\
      = & \beta_1 + \frac{\Er[x \epsilon |  u_i \geq -(\gamma_0 + \gamma_1 x_i +
          \gamma_2 z_i)]}{\var(x|d=1)} \neq \beta_1
    \end{align*}
  \item Assume $\epsilon, u$ jointly normally
    distributed with correlation $\rho$, then
    \begin{align*}
      \Er[\epsilon |  u_i \geq -(\gamma_0 + \gamma_1 x_i +
          \gamma_2 z_i)] = & \rho \sigma_\epsilon \frac{\phi(-(\gamma_0 + \gamma_1 x_i +
          \gamma_2 z_i))}{\Phi(-(\gamma_0 + \gamma_1 x_i +
          \gamma_2 z_i))} 
    \end{align*} 
  \item \cite{heckman1979} two-stage estimator:
    \begin{enumerate}
    \item Estimate $\hat{\gamma}_0,\hat{\gamma}_1,\hat{\gamma}_2$ using probit 
    \item Estimate $\hat{\beta}_0, \hat{\beta}_1, \widehat{\rho \sigma_\epsilon}$ by OLS
      regression of $y$ on $x$ and $\frac{\phi(-(\hat{\gamma}_0 + \hat{\gamma}_1 x_i +
        \hat{\gamma}_2 z_i))}{\Phi(-(\hat{\gamma}_0 + \hat{\gamma}_1 x_i +
        \hat{\gamma}_2 z_i))}$
    \end{enumerate}
  \item For reliable results important that a variable, like $z_i$, is
    included in selection equation and excluded from outcome equation
  \end{itemize}
\end{frame}

\section{Nonparametric models}

\section{Structural models}

\begin{frame}[allowframebreaks]
  \frametitle{References}
\bibliographystyle{jpe}
\bibliography{../326}
\end{frame}
  

\end{document}


