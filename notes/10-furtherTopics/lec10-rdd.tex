\input{../slideHeader}

\title{Regression discontinuity}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 326} 
\date{\today}


\providecommand{\bols}{{\hat{\beta}^{\mathrm{OLS}}}}
\providecommand{\biv}{{\hat{\beta}^{\mathrm{IV}}}}
\providecommand{\btsls}{{\hat{\beta}^{\mathrm{2SLS}}}}

\graphicspath{{figures/}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item \cite{ap2014} chapter 4
  \item \cite{lee2010}
  \end{itemize}  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Regression discontinuity}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{ap-fig4p1}    
\end{frame}
  
\begin{frame}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{ap-fig4p2}    
\end{frame}

\begin{frame}\frametitle{Regression discontinuity}
  \begin{itemize}
  \item Regression
    \[ 
      death_i = \beta_0 + \beta_1 LegalToDrink_i + \beta_2 age_i +
      \epsilon_i
    \]
  \item $LegalToDrink = \begin{cases} 1 & \text{ if } age \geq 21 \\
      0 & \text{ if } age < 21
    \end{cases}$ is a known, discontinuous function of age
  \item Assume $\Er[death | LegalToDrink=1, age]$ and
    $\Er[death|LegalToDrink=0, age]$ are continuous functions of age
  \item Then $\beta_1$ is an estimate of $\Er[death |
    LegalToDrink=1, age=21] - \Er[death | LegalToDrink=0, age=21]$,
    the causal effect of drinking becoming legal at age 21 on death
    rates
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Nonlinearity vs discontinuity}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{ap-fig4p3}    
  \end{center}
\end{frame}

\begin{frame}\frametitle{Nonlinearity vs discontinuity}
  \begin{itemize}
  \item Can be hard to tell nonlinearity from discontinuity
  \item Solution: flexibly model $\Er[death | LegalToDrink, age]$ as
    function of age
    \begin{itemize}
    \item Different slopes on each side of discontinuity:
      \[ y_i = \beta_0 + \beta_1 D_i + \beta_2 a_i +
        + \beta_3 D_i (a_i -21) + \epsilon_i
      \]
    \item Include powers of age :
      \[  y_i = \beta_0 + \beta_1 D_i + \beta_2 a_i + \beta_3 a_i^2 +
        + \beta_4 D_i (a_i - 21) +  \beta_5 D_i (a_i - 21)^2 + \epsilon_i
      \]
    \item Limit sample to small range around discontinuity
      ``bandwidth''
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{ap-fig4p4}    
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{ap-tab4p1}    
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{ap-fig4p5}    
  \end{center}
\end{frame}

\section{Using discontinuity as an instrument}

\begin{frame} \frametitle{Discontinuity as an instrument}
  \begin{itemize}
  \item Earlier regressions give good estimates of the effect of
    reaching the legal drinking age on mortality
  \item What is the causal effect of drinking (or binge drinking) on
    mortality?
  \item Idea : use discontinuity as instrument    
  \end{itemize}
\end{frame}

\subsection{\cite{carpenter2009}}
\begin{frame}\frametitle{\cite{carpenter2009}}
  ``The Effect of Alcohol Consumption on Mortality: 
  Regression Discontinuity Evidence from the  
  Minimum Drinking Age''
  \begin{itemize}
  \item Data on drinking and age from National Health Interview Survey
    (NHIS), 1997-2005
  \item Data on mortality and age from National Center for Health
    Statistics, 1997-2004   
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item Equation of interest:
    \[ mortality = \beta_0 + \beta_1 drinking + controls + \epsilon \]
  \item First stage :
    \[ drinking = \alpha_0 + \alpha_1 Legal + \alpha_2 age + controls
      + u \]
  \item Reduced form :
    \[ mortality = \pi_0 + \pi_1 Legal + \pi_2 age + controls + v \]
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{cd-fig1}    
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{cd-tab1}    
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{cd-fig3}    
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{cd-tab4}    
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{cd-fig4}    
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth, height=\textheight,
    keepaspectratio=true]{cd-tab5}    
  \end{center}
\end{frame}

\begin{frame}\frametitle{IV}
  \begin{itemize}
  \item Equation of interest:
    \[ mortality = \beta_0 + \beta_1 drinking + controls + \epsilon \]
  \item First stage :
    \[ drinking = \alpha_0 + \alpha_1 Legal + \alpha_2 age + controls
      + u \]
  \item Reduced form :
    \[ mortality = \pi_0 + \pi_1 Legal + \pi_2 age + controls + v \]
  \item First stage and reduced form estimated with different data,
    how to estimate $\beta_1$?
  \end{itemize}
\end{frame}

\section{General guidelines}

\begin{frame}
  \begin{itemize}
  \item Regression discontinuity useful when policy of interest is
    discontinuous function of some ``running variable''
    \begin{itemize}
    \item Examples : age, exam scores, elections
    \item Important that individuals cannot perfectly control being
      just above/below threshold (social assistance program with
      strict income limit would be problematic)
    \end{itemize}
  \item Report results graphically and in tables
  \item Report a range of specifications, flexibly controlling for the
    running variable
  \item See \cite{lee2010} 
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
\bibliographystyle{jpe}
\bibliography{../326}
\end{frame}
  

\end{document}

