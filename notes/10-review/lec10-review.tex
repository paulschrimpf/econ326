\input{../slideHeader}

\title{Review}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 326} 
\date{\today}


\providecommand{\bols}{{\hat{\beta}^{\mathrm{OLS}}}}
\providecommand{\biv}{{\hat{\beta}^{\mathrm{IV}}}}
\providecommand{\btsls}{{\hat{\beta}^{\mathrm{2SLS}}}}

\graphicspath{{figures/}}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \tableofcontents  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Asymptotics}

\begin{frame}[allowframebreaks]
  \frametitle{Asymptotics}
  \begin{itemize}
  \item Idea: use limit of distribution of estimator as $n \to \infty$
    to approximate finite sample distribution of estimator
  \item $W_n$ \alert{converges in probability} to $\theta$ if for
    every $\epsilon > 0$, 
    \[ \lim_{n \to \infty} \Pr\left( \abs{W_n - \theta} > \epsilon
    \right) = 0 \]
    denote by $\plim W_n = \theta$ or $W_n \inprob \theta$
  \item Law of large numbers: if $y_1, ..., y_n$ are
    not too dependent and $\var(y_i)<\infty$, then $\bar{y} \inprob
    \Er[Y]$ 
    \begin{itemize}
    \item $\plim g(W_n) = g(\plim W_n)$ if $g$ is continuous
      (\alert{continuous mapping theorem (CMT)})
    \item If $W_n \inprob \omega$ and $Z_n \inprob \zeta$, then
      (\alert{Slutsky's lemma})
      \begin{itemize} 
      \item $W_n + Z_n \inprob \omega + \zeta$
      \item $W_n Z_n \inprob \omega \zeta$
      \item $\frac{W_n}{Z_n} \inprob \frac{\omega}{\zeta}$
      \end{itemize}
    \end{itemize}
  \item $W_n$ is a \alert{consistent} estimate of $\theta$ if $W_n
    \inprob \theta$
  \item $W_n$ \alert{converges in distribution} to $W$, written $W_n \indist
    W$, if $\lim_{n \to \infty} F_n(x) = F(x)$ for all $x$ where $F$
    is continuous 
  \item \alert{Central limit theorem}: Let $\{y_1, ..., y_n\}$ be not
    too dependent with mean $\mu$ and variance $\sigma^2$ then $Z_n =
    \sqrt{n} \frac{\bar{y}_n - \mu}{\sigma}$ converges in distribution
    to a standard normal random variable
    \begin{itemize}
    \item If $W_n \indist W$, then $g(W_n) \indist g(W)$ for
      continuous $g$ (\alert{continuous mapping theorem (CMT)})
    \item Slutsky's theorem: If $W_n \indist W$ and $Z_n \inprob c$, then (i) $W_n +
      Z_n \indist W + c$, (ii) $W_n Z_n \indist c W$, and (iii)
      $W_n/Z_n \indist W/c$
    \end{itemize}
  \end{itemize}
\end{frame}

\section{OLS}

\begin{frame}[allowframebreaks]
  \frametitle{OLS}
  \begin{align*}
    y_i = \beta_0 + \beta_1 x_{1,i} + \beta_2 x_{2,i} + \cdots +
    \beta_k x_{k,i} + \epsilon_i 
  \end{align*}
  \begin{itemize} 
  \item Assumptions:
    \begin{enumerate}
    \item[MLR.1] (linear model) 
    \item[MLR.2] (independence) $\{(x_{1,i},x_{2,i},y_i)\}_{i=1}^n$ is
      an independent random sample
    \item[MLR.3] (rank condition) no multicollinearity: no $x_{j,i}$ is
      constant and there is no exact linear relationship among the $x_{j,i}$
    \item[MLR.4] (exogeneity) $\Er[\epsilon_i | x_{1,i},..., x_{k,i}]
      = 0$
    \item[MLR.5] (homoskedasticity) $\var(\epsilon_i | X) =
      \sigma^2_\epsilon$
    \item[MLR.6] $\epsilon_i |X \sim N(0,\sigma_\epsilon^2)$
    \end{enumerate}
  \item Unbiased if 1-4
  \item Consistent under 1, 3 and (2') observations are not too
    dependent and (4') $\Er[\epsilon_i x_{j,i}] = 0$
  \item Asymptotically normal under 1, 2', 3, 4'
  \item No need to assume 5, just use heteroskedasticity robust
    standard errors
    \begin{itemize}
    \item If observations are dependent through time or through
      clustering must modify standard errors
    \end{itemize}
  \item Interpretation of coefficients: $\beta_j$ is effect of
    $x_{j,i}$ holding the other $x$'s constant
  \item Estimates satisfy OLS first order conditions
    \[  \sumin \left(y_i - \hat{\beta}_0 - \hat{\beta}_1 x_{1,i} -
      \cdots - \hat{\beta}_k x_{k,i} \right) x_{j,i} = 0  \text{ for
    } j =1,...,k \]
    or
    \[ \sumin \hat{\epsilon}_i x_{j,i} =   \text{ for }
    j=1,2,...,k \] 
  \item Can also write estimates using partitioned regression
    \begin{itemize}
    \item Regress $x_{1,i}$ on other regressors
      \[ x_{1,i} = \hat{\gamma}_0 + \hat{\gamma}_2 x_{2,i} + \cdots +
      \hat{\gamma}_k x_{k,i} + \tilde{x}_{1,i} \]
      where $\tilde{x}_{1,i}$ is the OLS residual
    \item Then 
      \[ \hat{\beta}_1 = \frac{\sumin \tilde{x}_{1,i} y_i}{\sumin
        \tilde{x}_{1,i}^2} \]
    \end{itemize}
  \end{itemize}
\end{frame}


\section{IV}

\begin{frame}[allowframebreaks]\frametitle{IV}
  \begin{itemize}
  \item Model
    \begin{equation}
      y_i = \beta_0 + \beta_1 x_{1,i} + \cdots + \beta_k x_{k,i} +
      \beta_{k+1} w_{1,i} + \cdots + \beta_{k+r} w_{r,i} + \epsilon_i \label{mlin}
    \end{equation}
    with instruments $z_{1,i}, ..., z_{m,i}$
  \item Assumptions:    
    \begin{itemize}
    \item[IV.1] Linearity: (\ref{mlin}) holds
    \item[IV.2] Independent observations
    \item[IV.3] Relevance (rank condition): $m \geq k$ and (loosely
      speaking) each $x_{j,i}$ is correlated with some $z_{l,i}$
    \item[IV.4] Exogeneity: $\Er[w_{s,i} \epsilon_i] = 0$ for
      $s=1,..,r$ and $\Er[z_{l,i} \epsilon_i ] = 0$ for $l=1,...,m$
    \end{itemize}
  \item Estimate by two stage least squares
    \begin{enumerate}
    \item Regress $x$'s on $z$'s and $w$'s
    \item Regress $y$ on $\hat{x}$'s and $w$'s
    \end{enumerate}
    \begin{itemize}
    \item Should check relevance in the first stage
    \item Reduced form is regression of $y$ on $z$ and $w$
    \item IV estimate $\simeq$ reduced form divided by first stage
    \end{itemize}
  \item If IV.1-IV.4, then 2SLS is consistent and asymptotically
    normal
  \item Need to use IV instead of OLS when we don't believe
    $\Er[x\epsilon]=0$
    \begin{itemize}
    \item i.e. the model we want to estimate is not the population
      regression for the data we have
    \item instead we want a causal effect or parameters from an
      economic model
    \end{itemize}
  \item Comparing OLS and IV estimates
  \end{itemize}
\end{frame}


%\begin{frame}[allowframebreaks]
%  \frametitle{References}
%\bibliographystyle{jpe}
%\bibliography{../326}
%\end{frame}
  

\end{document}


