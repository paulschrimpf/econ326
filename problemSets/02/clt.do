/*
   14.32 Problem Set 1
   Monte-Carlo Evidence of the CLT
   February 18, 2009
   Paul Schrimpf
*/

clear
clear programs
set more off

// define a command that runs the simulations
program define simNormal, rclass
  version 10.1
  syntax [, obs(integer 1) ]
  drop _all
  set obs `obs'
  tempvar x
  gen `x' = rnormal()
  summarize `x'
  return scalar mean = r(mean)
  return scalar Var  = r(Var)
end

foreach s of numlist 8 32 128 { // sample sizes
  simulate meanx=r(mean) varx=r(Var), reps(5000): simNormal, obs(`s')
  summarize meanx varx
  hist meanx, bin(25) percent name(m`s') ///
   subtitle("Histogram of mean from Normal(0,1): 5000 times `s' obs")
  graph export nclt`s'.eps, replace
  hist varx, bin(25) percent name(v`s') ///
   subtitle("Histogram of variance from Normal(0,1): 5000 times `s' obs")
  graph export nv`s'.eps, replace
  drop meanx varx

}


