rm(list=ls())   ## clear workspace
graphics.off()  ## close graphs

## load.fun(x) will load library 'x' if it is installed. If 'x' has
## not been installed, it will install it. Understanding this code is
## not necessary
## source: http://r.789695.n4.nabble.com/Install-package-automatically-if-not-there-tp2267532p2267659.html
load.fun <- function(x) { 
  x <- as.character(substitute(x)) 
  if(isTRUE(x %in% .packages(all.available=TRUE))) { 
    eval(parse(text=paste("require(", x, ")", sep=""))) 
  } else { 
    #update.packages()  ## good idea, but may take some time. can
    ## usually be safely skipped
    eval(parse(text=paste("install.packages('", x, "')", sep="")))
    eval(parse(text=paste("require(", x, ")", sep=""))) 
  } 
} 

## download data
if (!file.exists("final5.dta")) { ## only download if have not already
  message("Downloading data") ## display a message
  download.file(url="http://economics.mit.edu/files/1358",
                destfile="final5.dta",mode="wb")
} else {
  message("final5.dta already exists, not re-downloading")
}
## load data
load.fun(foreign) ## for reading .dta files
aldata <- read.dta("final5.dta")

## data fixes
## fix scores > 100
aldata$avgmath[aldata$avgmath>100 & !is.na(aldata$avgmath)] <-
  aldata$avgmath[aldata$avgmath>100 & !is.na(aldata$avgmath)] - 100
aldata$avgverb[aldata$avgverb>100 & !is.na(aldata$avgverb)] <-
  aldata$avgverb[aldata$avgverb>100 & !is.na(aldata$avgverb)] - 100
## classes with size 0 should not have scores
aldata$avgmath[aldata$mathsize==0 ] <- NA
aldata$avgverb[aldata$verbsize==0 ] <- NA

## delete small enrollment
aldata <- aldata[aldata$c_size>=5, ] ## selects all rows from aldata with
## c_size >= 5 and all columns
## by law classsize <= 40, get rid of big cheaters
aldata <- aldata[aldata$classize<45, ]

## get rid of those missing test score
aldata <- aldata[!is.na(aldata$avgmath) & !is.na(aldata$avgverb), ]

## summary statistics -- easy version, but without 0.1 & 0.9 quantile
summary(aldata[, c("classize","c_size","tipuach",
                   "verbsize","mathsize", "avgverb",
                   "avgmath")])
## summary statistics with quantiles 
table <- matrix(0,nrow=7,ncol=7)
vnames <- c("classize","c_size","tipuach", "verbsize","mathsize",
            "avgverb", "avgmath")
for(i in 1:length(vnames)) {
  x <- aldata[,vnames[i]]
  table[i,] <- c(mean(x), sd(x),
                 quantile(x, probs=c(0.1,0.25, 0.5, 0.75, 0.9)))
}
colnames(table) <- c("Mean","SD","0.1", "0.25" ,
                     "0.5", "0.75", "0.9")
rownames(table) <- vnames
print(table,digits=3)


## regression of avgmath on classize
mathReg <- lm(avgmath ~ classize, data = aldata)
## lm stands for "linear model." Its first argument is a formula,
## for a regression of y on x the formula should be "y ~ x" for y on
## x and z the formula would be "y ~ x + z". The second argument of
## lm, data=aldata tells lm to use the variables (columns) of the
## dataframe aldata to calculate the regression

summary(mathReg) ## show summary of regression results
