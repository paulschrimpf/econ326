/*
   PS2, question 3 of 14.32
   Angrist and Lavy -- Class size and test scores
   February 1, 2009
   Paul Schrimpf
*/
capture log close
log using angristLavy.log, text replace
clear
set mem 50m
// data is from http://econ-www.mit.edu/faculty/angrist/data1
// more specifically: (as of February 13, 2008)
// !wget http://econ-www.mit.edu/files/1358
// !mv 1358 final5.dta
use final5, clear
label var classize "Class size"
label var c_size "Enrollment"
label var tipuach "Percent disadvantaged"
label var mathsize "Math size"
label var verbsize "Reading size"
label var avgmath "Average math"
label var avgverb "Average verbal"
// non-obvious data fixes
// fix two data entry errors (no score can be > 100)
replace avgmath = avgmath - 100 if avgmath > 180
replace avgverb = avgverb - 100 if avgverb > 180
// classes of 0 should not have avg scores
replace avgmath = . if mathsize==0
replace avgverb = . if verbsize==0
// get rid of really small enrollments
keep if c_size>=5
// by law, classize should be at most 40, get rid of big cheaters
keep if classize<45
// get rid of those missing test score
drop if missing(avgmath) | missing(avgverb)

// part 1
tabstat classize c_size tipuach verbsize mathsize avgverb avgmath ///
  , statistics(n mean sd p10 p25 median p75 p90)

// part 2
reg avgmath classize
reg avgverb classize

log close
