## install needed packages
if (!require(lfe)) install.packages("lfe")
if (!require(foreign)) install.packages("foreign")
library(lfe)
library(foreign)
## You must download data from AEJ:Applied website,
## https://www.aeaweb.org/articles?id=10.1257/app.5.2.179
## and extract from zip file
txt <- read.dta("estdata.dta")
## Drop some useless variables
txt <- txt[,-grep("^(t\\d|st\\d|stt\\d|stb\\d|mon\\d)", names(txt))]
txt$strongban <- with(txt, second!=1 & agelimit!=1 & txmsban==1)
txt$fullban <- with(txt,strongban & HHBAN)
txt$strongtxtban <- with(txt,strongban & HHBAN==0)
txt$weakban <- with(txt, !(strongban) & txmsban==1)
txt$state <- as.factor(txt$state)
txt$w <- ifelse(is.na(txt$pop),0,txt$pop)

## Column 1 of table 3
summary(felm(laccidentsvso2 ~ txmsban + lpop + lunemp + permale2 +
               lrgastax |
               state + as.factor(time),
             clustervar="state" ,data=txt, weights=txt$w))

## Column 5 of table 3
summary(felm(laccidentsvso2 ~ strongban + weakban + lpop + lunemp + permale2 +
               lrgastax + laccidentmv2 |
               state + as.factor(time) + state:time,
             clustervar="state" ,data=txt, weights=txt$w))

